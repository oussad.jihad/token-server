package fr.jihadoussad.tokenserver.api;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import fr.jihadoussad.tools.api.logger.Slf4jMDCFilterConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = "fr.jihadoussad.tokenserver.*", basePackageClasses = { Slf4jMDCFilterConfiguration.class })
@EntityScan(basePackages = "fr.jihadoussad.tokenserver.core.*")
@EnableJpaRepositories(basePackages = "fr.jihadoussad.tokenserver.core.*")
@EnableEncryptableProperties
public class TokenServerApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(final SpringApplicationBuilder builder) {
        return builder.sources(TokenServerApplication.class);
    }

    public static void main(final String[] args) {
        SpringApplication.run(TokenServerApplication.class, args);
    }
}
