package fr.jihadoussad.tokenserver.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.jihadoussad.tokenserver.core.repositories.InvalidTokenRepository;
import org.junit.jupiter.api.AfterEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class CommonIntegrationTest {

    protected static final String GENERATE_URL = "/generate";
    protected static final String PAYLOAD_URL = "/payload";
    protected static final String VERIFY_URL = "/verify";
    protected static final String INVALIDATE_URL = "/invalidate";

    @Autowired
    protected MockMvc mvc;

    @Autowired
    protected InvalidTokenRepository invalidTokenRepository;

    @Autowired
    protected ObjectMapper objectMapper;

    @AfterEach
    public void cleanUp() {
        invalidTokenRepository.deleteAll();
        invalidTokenRepository.flush();
    }

    protected String jsonResponse(final String urlTemplate, final Object input, final ResultMatcher resultMatcher) throws Exception {
        return mvc.perform(post(urlTemplate)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(input)))
                .andExpect(resultMatcher)
                .andReturn()
                .getResponse()
                .getContentAsString();
    }
}
