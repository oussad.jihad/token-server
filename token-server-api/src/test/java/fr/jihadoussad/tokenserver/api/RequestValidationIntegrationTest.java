package fr.jihadoussad.tokenserver.api;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class RequestValidationIntegrationTest extends CommonIntegrationTest {

    @Test
    public void generate_httpRequestMethodNotSupportedExceptionTest() throws Exception {
        mvc.perform(get(GENERATE_URL)
                .header("Access-Control-Request-Method", "GET"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void generate_httpMediaTypeNotSupportedTest() throws Exception {
        mvc.perform(post(GENERATE_URL)
                .header("Access-Control-Request-Method", "POST"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void generate_missingRequestBodyTest() throws Exception {
        mvc.perform(post(GENERATE_URL)
                .header("Access-Control-Request-Method", "POST")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void payload_httpRequestMethodNotSupportedExceptionTest() throws Exception {
        mvc.perform(get(PAYLOAD_URL)
                .header("Access-Control-Request-Method", "GET"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void payload_httpMediaTypeNotSupportedTest() throws Exception {
        mvc.perform(post(PAYLOAD_URL)
                .header("Access-Control-Request-Method", "POST"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void payload_missingRequestBodyTest() throws Exception {
        mvc.perform(post(PAYLOAD_URL)
                .header("Access-Control-Request-Method", "POST")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void verify_httpRequestMethodNotSupportedExceptionTest() throws Exception {
        mvc.perform(get(VERIFY_URL)
                .header("Access-Control-Request-Method", "GET"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void verify_httpMediaTypeNotSupportedTest() throws Exception {
        mvc.perform(post(VERIFY_URL)
                .header("Access-Control-Request-Method", "POST"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void verify_missingRequestBodyTest() throws Exception {
        mvc.perform(post(VERIFY_URL)
                .header("Access-Control-Request-Method", "POST")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void invalidate_httpRequestMethodNotSupportedExceptionTest() throws Exception {
        mvc.perform(get(INVALIDATE_URL)
                .header("Access-Control-Request-Method", "GET"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void invalidate_httpMediaTypeNotSupportedTest() throws Exception {
        mvc.perform(post(INVALIDATE_URL)
                .header("Access-Control-Request-Method", "POST"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void invalidate_missingRequestBodyTest() throws Exception {
        mvc.perform(post(INVALIDATE_URL)
                .header("Access-Control-Request-Method", "POST")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }
}
