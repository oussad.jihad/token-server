package fr.jihadoussad.tokenserver.api;

import fr.jihadoussad.tokenserver.contract.input.Token;
import fr.jihadoussad.tokenserver.contract.input.GenerateTokenInput;
import fr.jihadoussad.tokenserver.contract.input.VerifyTokenInput;
import fr.jihadoussad.tokenserver.core.entities.InvalidToken;
import fr.jihadoussad.tokenserver.test.TestHelper;
import fr.jihadoussad.tools.api.response.ErrorOutput;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.impl.DefaultClaims;
import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;
import java.util.Map;

import static fr.jihadoussad.tools.api.response.ResponseCode.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class TokenServerApplicationIntegrationTest extends CommonIntegrationTest {

    @Test
    public void generateTokenWhenSecretKeyNullTest() throws Exception {
        final GenerateTokenInput input = new GenerateTokenInput();
        final ErrorOutput response = objectMapper.readValue(jsonResponse(GENERATE_URL, input, status().isBadRequest()), ErrorOutput.class);

        assertThat(response.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
        assertThat(response.getMessage()).isEqualTo(MISSING_MANDATORY_FIELD.message + "secret key null");
    }

    @Test
    public void generateTokenWithSecretKeyNotEncodedTest() throws Exception {
        final GenerateTokenInput input = new GenerateTokenInput();
        input.setSecretKey("badSecretKey");
        final ErrorOutput response = objectMapper.readValue(jsonResponse(GENERATE_URL, input, status().isBadRequest()), ErrorOutput.class);

        assertThat(response.getCode()).isEqualTo(INVALID_SECRET_KEY.code);
        assertThat(response.getMessage()).isEqualTo(INVALID_SECRET_KEY.message);
    }

    @Test
    public void generateTokenWithSecretKeySizeLessThan256BitsTest() throws Exception {
        final GenerateTokenInput input = new GenerateTokenInput();
        input.setSecretKey(Base64.getEncoder().encodeToString("badSecretKey".getBytes(StandardCharsets.UTF_8)));
        final ErrorOutput response = objectMapper.readValue(jsonResponse(GENERATE_URL, input, status().isBadRequest()), ErrorOutput.class);

        assertThat(response.getCode()).isEqualTo(INVALID_SECRET_KEY.code);
        assertThat(response.getMessage()).isEqualTo(INVALID_SECRET_KEY.message);
    }

    @Test
    public void generateTokenWhenExpirationNullTest() throws Exception {
        final GenerateTokenInput input = new GenerateTokenInput();
        input.setSecretKey(TestHelper.SECRET_KEY);
        final ErrorOutput response = objectMapper.readValue(jsonResponse(GENERATE_URL, input, status().isBadRequest()), ErrorOutput.class);

        assertThat(response.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
        assertThat(response.getMessage()).isEqualTo(MISSING_MANDATORY_FIELD.message + "expiration null");
    }

    @Test
    public void generateTokenWhenIssuerNullTest() throws Exception {
        final GenerateTokenInput input = new GenerateTokenInput();
        input.setSecretKey(TestHelper.SECRET_KEY);
        input.setExpiration(1000L);
        final ErrorOutput response = objectMapper.readValue(jsonResponse(GENERATE_URL, input, status().isBadRequest()), ErrorOutput.class);

        assertThat(response.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
        assertThat(response.getMessage()).isEqualTo(MISSING_MANDATORY_FIELD.message + "issuer null");
    }

    @Test
    public void generateTokenWhenSubjectNullTest() throws Exception {
        final GenerateTokenInput input = new GenerateTokenInput();
        input.setSecretKey(TestHelper.SECRET_KEY);
        input.setExpiration(1000L);
        input.setIssuer("issuer");
        final ErrorOutput response = objectMapper.readValue(jsonResponse(GENERATE_URL, input, status().isBadRequest()), ErrorOutput.class);

        assertThat(response.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
        assertThat(response.getMessage()).isEqualTo(MISSING_MANDATORY_FIELD.message + "subject null");
    }

    @Test
    public void extractPayloadWhenTokenNullTest() throws Exception {
        final Token token = new Token();
        final ErrorOutput response = objectMapper.readValue(jsonResponse(PAYLOAD_URL, token, status().isBadRequest()), ErrorOutput.class);

        assertThat(response.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
        assertThat(response.getMessage()).isEqualTo(MISSING_MANDATORY_FIELD.message + "token null");
    }

    @Test
    public void extractPayloadWhenTokenInvalidTest() throws Exception {
        final Token token = new Token();
        token.setToken("token");

        assertThat(jsonResponse(PAYLOAD_URL, token, status().isOk())).isNullOrEmpty();
    }

    @Test
    public void extractPayloadWhenPayloadInvalidTest() throws Exception {
        final Token token = new Token();
        token.setToken("header.fakePayload.signature");

        assertThat(jsonResponse(PAYLOAD_URL, token, status().isOk())).isNullOrEmpty();
    }

    @Test
    public void extractPayloadTest() throws Exception {
        final Token token = new Token();
        token.setToken(TestHelper.TOKEN);
        final Claims output = objectMapper.readValue(jsonResponse(PAYLOAD_URL, token, status().isOk()), DefaultClaims.class);

        assertThat(output).isNotNull();
        assertThat(output.getIssuer()).isEqualTo(TestHelper.ISSUER);
        assertThat(output.getSubject()).isEqualTo(TestHelper.SUBJECT);
    }

    @Test
    public void verifyTokenWhenTokenNullTest() throws Exception {
        final VerifyTokenInput verifyTokenInput = new VerifyTokenInput();
        final ErrorOutput response = objectMapper.readValue(jsonResponse(VERIFY_URL, verifyTokenInput, status().isBadRequest()), ErrorOutput.class);

        assertThat(response.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
        assertThat(response.getMessage()).isEqualTo(MISSING_MANDATORY_FIELD.message + "token null");
    }


    @Test
    public void verifyTokenWhenSecretKeyNullTest() throws Exception {
        final VerifyTokenInput verifyTokenInput = new VerifyTokenInput();
        verifyTokenInput.setToken("token");
        final ErrorOutput response = objectMapper.readValue(jsonResponse(VERIFY_URL, verifyTokenInput, status().isBadRequest()), ErrorOutput.class);

        assertThat(response.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
        assertThat(response.getMessage()).isEqualTo(MISSING_MANDATORY_FIELD.message + "secret key null");
    }

    @Test
    public void verifyTokenWithSecretKeyNotEncodedTest() throws Exception {
        final VerifyTokenInput verifyTokenInput = new VerifyTokenInput();
        verifyTokenInput.setToken("token");
        verifyTokenInput.setSecretKey("badSecretKey");
        final ErrorOutput response = objectMapper.readValue(jsonResponse(VERIFY_URL, verifyTokenInput, status().isBadRequest()), ErrorOutput.class);

        assertThat(response.getCode()).isEqualTo(INVALID_SECRET_KEY.code);
        assertThat(response.getMessage()).isEqualTo(INVALID_SECRET_KEY.message);
    }

    @Test
    public void verifyTokenWithSecretKeySizeLessThan256BitsTest() throws Exception {
        final VerifyTokenInput verifyTokenInput = new VerifyTokenInput();
        verifyTokenInput.setToken("token");
        verifyTokenInput.setSecretKey(Base64.getEncoder().encodeToString("badSecretKey".getBytes(StandardCharsets.UTF_8)));
        final ErrorOutput response = objectMapper.readValue(jsonResponse(VERIFY_URL, verifyTokenInput, status().isBadRequest()), ErrorOutput.class);

        assertThat(response.getCode()).isEqualTo(INVALID_SECRET_KEY.code);
        assertThat(response.getMessage()).isEqualTo(INVALID_SECRET_KEY.message);
    }

    @Test
    public void verifyTokenWithTokenInvalidTest() throws Exception {
        final VerifyTokenInput verifyTokenInput = new VerifyTokenInput();
        verifyTokenInput.setSecretKey(TestHelper.SECRET_KEY);
        verifyTokenInput.setToken("token");
        final ErrorOutput response = objectMapper.readValue(jsonResponse(VERIFY_URL, verifyTokenInput, status().isForbidden()), ErrorOutput.class);

        assertThat(response.getCode()).isEqualTo(INVALID_TOKEN.code);
        assertThat(response.getMessage()).isEqualTo(INVALID_TOKEN.message);
    }

    @Test
    public void verifyTokenWithTokenExpiredTest() throws Exception {
        final VerifyTokenInput verifyTokenInput = new VerifyTokenInput();
        verifyTokenInput.setSecretKey(TestHelper.SECRET_KEY);
        verifyTokenInput.setToken(TestHelper.TOKEN_EXPIRED);
        final ErrorOutput response = objectMapper.readValue(jsonResponse(VERIFY_URL, verifyTokenInput, status().isForbidden()), ErrorOutput.class);

        assertThat(response.getCode()).isEqualTo(EXPIRED_TOKEN.code);
        assertThat(response.getMessage()).isEqualTo(EXPIRED_TOKEN.message);
    }

    @Test
    public void invalidateTokenWithTokenNullTest() throws Exception {
        final Token jwt = new Token();
        final ErrorOutput response = objectMapper.readValue(jsonResponse(INVALIDATE_URL, jwt, status().isBadRequest()), ErrorOutput.class);

        assertThat(response.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
        assertThat(response.getMessage()).isEqualTo(MISSING_MANDATORY_FIELD.message + "token null");
    }

    @Test
    public void invalidateTokenWithTokenInvalidTest() throws Exception {
        final Token jwt = new Token();
        jwt.setToken("token");
        jsonResponse(INVALIDATE_URL, jwt, status().isOk());
    }

    @Test
    public void invalidateTokenWithTokenAlreadyInvalidTest() throws Exception {
        final InvalidToken invalidToken = new InvalidToken();
        invalidToken.setToken(TestHelper.TOKEN);
        invalidTokenRepository.save(invalidToken);
        invalidTokenRepository.flush();

        final Token jwt = new Token();
        jwt.setToken(TestHelper.TOKEN);
        jsonResponse(INVALIDATE_URL, jwt, status().isOk());
    }

    @Test
    public void generateVerifyAndInvalidateTokenTest() throws Exception {
        // Generate a token

        final GenerateTokenInput input = new GenerateTokenInput();
        input.setSecretKey(TestHelper.SECRET_KEY);
        input.setExpiration(10000L);
        input.setIssuer("issuer");
        input.setSubject("subject");
        input.setClaims(Map.of("privileges", List.of("admin", "user")));
        final String jwtResponse = jsonResponse(GENERATE_URL, input, status().isOk());

        assertThat(jwtResponse).isNotEmpty();

        // Verify token validity

        final VerifyTokenInput verifyTokenInput = new VerifyTokenInput();
        verifyTokenInput.setToken(jwtResponse);
        verifyTokenInput.setSecretKey(TestHelper.SECRET_KEY);
        final Claims verifyResponse = objectMapper.readValue(jsonResponse(VERIFY_URL, verifyTokenInput, status().isOk()), DefaultClaims.class);

        assertThat(verifyResponse)
                .containsEntry("sub", "subject")
                .containsEntry("iss", "issuer")
                .containsEntry("privileges", List.of("admin", "user"))
                .containsKey("exp");

        // Invalidate the token

        final Token jwt = new Token();
        jwt.setToken(jwtResponse);
        jsonResponse(INVALIDATE_URL, jwt, status().isOk());

        // Verify token invalidity

        final ErrorOutput response = objectMapper.readValue(jsonResponse(VERIFY_URL, verifyTokenInput, status().isForbidden()), ErrorOutput.class);
        assertThat(response.getCode()).isEqualTo(INVALID_TOKEN.code);
        assertThat(response.getMessage()).isEqualTo(INVALID_TOKEN.message);
    }
}
