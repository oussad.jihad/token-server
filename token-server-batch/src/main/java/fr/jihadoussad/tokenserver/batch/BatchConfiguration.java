package fr.jihadoussad.tokenserver.batch;

import fr.jihadoussad.tokenserver.core.entities.InvalidToken;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.data.RepositoryItemWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

    private final InvalidTokenReader invalidTokenReader;

    public final JobBuilderFactory jobBuilderFactory;

    public final StepBuilderFactory stepBuilderFactory;

    public BatchConfiguration(final InvalidTokenReader invalidTokenReader,
                              final JobBuilderFactory jobBuilderFactory, final StepBuilderFactory stepBuilderFactory) {
        this.invalidTokenReader = invalidTokenReader;
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
    }

    @Bean
    public Job deleteInvalidTokenJob(final JobExecutionListener listener, final Step step) {
        return jobBuilderFactory.get("deleteInvalidTokenJob")
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .flow(step)
                .end()
                .build();
    }

    @Bean
    public Step step(final RepositoryItemWriter<InvalidToken> itemWriter) {
        return stepBuilderFactory.get("step")
                .<InvalidToken, InvalidToken> chunk(1)
                .reader(invalidTokenReader)
                .writer(itemWriter)
                .build();
    }
}
