package fr.jihadoussad.tokenserver.batch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@ComponentScan(basePackages = "fr.jihadoussad.tokenserver.*")
@EntityScan(basePackages = "fr.jihadoussad.tokenserver.core.*")
@EnableJpaRepositories(basePackages = "fr.jihadoussad.tokenserver.core.*")
@EnableScheduling
public class InvalidTokenPurgeApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(final SpringApplicationBuilder builder) {
        return builder.sources(InvalidTokenPurgeApplication.class);
    }

    public static void main(final String[] args) {
        SpringApplication.run(InvalidTokenPurgeApplication.class, args);
    }
}
