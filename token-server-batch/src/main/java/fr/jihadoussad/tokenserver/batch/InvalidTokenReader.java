package fr.jihadoussad.tokenserver.batch;

import fr.jihadoussad.tokenserver.contract.TokenService;
import fr.jihadoussad.tokenserver.contract.exceptions.ContractValidationException;
import fr.jihadoussad.tokenserver.contract.input.Token;
import fr.jihadoussad.tokenserver.core.entities.InvalidToken;
import fr.jihadoussad.tokenserver.core.repositories.InvalidTokenRepository;
import io.jsonwebtoken.Claims;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.data.RepositoryItemReader;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class InvalidTokenReader extends RepositoryItemReader<InvalidToken> {

    private final Logger logger = LoggerFactory.getLogger(InvalidTokenReader.class);

    private final InvalidTokenRepository invalidTokenRepository;

    private final TokenService tokenService;

    public InvalidTokenReader(final InvalidTokenRepository invalidTokenRepository, final TokenService tokenService) {
        super();
        this.setRepository(invalidTokenRepository);
        this.setSort(Map.of("id", Sort.Direction.DESC));
        this.invalidTokenRepository = invalidTokenRepository;
        this.tokenService = tokenService;
    }


    @Override
    protected List<InvalidToken> doPageRead() {
        return invalidTokenRepository.findAll()
                .stream()
                .filter(invalidToken -> extractExpiration(invalidToken.getToken()).isBefore(LocalDateTime.now(ZoneOffset.UTC)))
                .collect(Collectors.toList());
    }

    private LocalDateTime extractExpiration(final String token) {
        LocalDateTime expiration = LocalDateTime.now(ZoneOffset.UTC).minusDays(1);
        try {
            final Token jwt = new Token();
            jwt.setToken(token);
            final Claims payload = tokenService.extractPayload(jwt);

            if (payload == null || payload.getExpiration() == null) {
                logger.info("The token is already invalid");
                return expiration;
            }

            expiration = payload.getExpiration().toInstant().atZone(ZoneOffset.UTC).toLocalDateTime();
        } catch (final ContractValidationException e) {
            logger.warn("Null token is stored into database");
        }

        return expiration;
    }
}
