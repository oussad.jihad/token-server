package fr.jihadoussad.tokenserver.batch;

import fr.jihadoussad.tokenserver.core.entities.InvalidToken;
import fr.jihadoussad.tokenserver.core.repositories.InvalidTokenRepository;
import org.springframework.batch.item.data.RepositoryItemWriter;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class InvalidTokenWriter extends RepositoryItemWriter<InvalidToken> {

    private final InvalidTokenRepository invalidTokenRepository;

    public InvalidTokenWriter(final InvalidTokenRepository invalidTokenRepository) {
        super();
        this.setRepository(invalidTokenRepository);
        this.invalidTokenRepository = invalidTokenRepository;
    }

    @Override
    public void write(List<? extends InvalidToken> items) {
        invalidTokenRepository.deleteAll(items);
        invalidTokenRepository.flush();
    }
}
