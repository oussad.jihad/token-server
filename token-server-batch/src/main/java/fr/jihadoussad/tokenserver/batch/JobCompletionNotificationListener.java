package fr.jihadoussad.tokenserver.batch;

import fr.jihadoussad.tokenserver.core.entities.InvalidToken;
import fr.jihadoussad.tokenserver.core.repositories.InvalidTokenRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class JobCompletionNotificationListener extends JobExecutionListenerSupport {

    private final Logger logger = LoggerFactory.getLogger(JobCompletionNotificationListener.class);

    private final InvalidTokenRepository invalidTokenRepository;

    public JobCompletionNotificationListener(final InvalidTokenRepository invalidTokenRepository) {
        this.invalidTokenRepository = invalidTokenRepository;
    }

    @Override
    public void beforeJob(final JobExecution jobExecution) {
        logger.info("!!! JOB STARTED!");
        checkInvalidTokenDB();
    }

    @Override
    public void afterJob(final JobExecution jobExecution) {
        if (jobExecution.getStatus() == BatchStatus.COMPLETED) {
            logger.info("!!! JOB FINISHED! Time to verify the results");
            checkInvalidTokenDB();
        } else {
            logger.error("!!! JOB FINISHED with status: {}", jobExecution.getExitStatus().getExitCode());
        }
    }

    private void checkInvalidTokenDB() {
        final List<InvalidToken> invalidTokens = invalidTokenRepository.findAll();
        if (invalidTokens.isEmpty()) {
            logger.info("Nothing found in invalid token database");
        } else {
            invalidTokens.forEach(invalidToken -> logger.info("Found invalid token with id <" + invalidToken.getId() + "> in the database."));
        }
    }
}
