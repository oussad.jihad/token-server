package fr.jihadoussad.tokenserver.batch;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.jihadoussad.tokenserver.core.entities.InvalidToken;
import fr.jihadoussad.tokenserver.core.repositories.InvalidTokenRepository;
import fr.jihadoussad.tokenserver.test.TestHelper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.impl.DefaultClaims;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.batch.test.JobRepositoryTestUtils;
import org.springframework.batch.test.context.SpringBatchTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBatchTest
@EnableAutoConfiguration
@ContextConfiguration(classes = { InvalidTokenPurgeApplication.class })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
@ActiveProfiles("test")
public class InvalidTokenPurgeIntegrationTest {

    @Autowired
    private InvalidTokenRepository invalidTokenRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private JobLauncherTestUtils jobLauncherTestUtils;

    @Autowired
    private JobRepositoryTestUtils jobRepositoryTestUtils;

    @AfterEach
    public void cleanUp() {
        invalidTokenRepository.deleteAll();
        invalidTokenRepository.flush();
        jobRepositoryTestUtils.removeJobExecutions();
    }

    @Test
    public void purgeFakeTokenTest() throws Exception {
        // GIVEN
        final InvalidToken invalidFakeToken = new InvalidToken();
        final String header = Base64.getEncoder().encodeToString("header".getBytes(StandardCharsets.UTF_8));
        final String payload = Base64.getEncoder().encodeToString("payload".getBytes(StandardCharsets.UTF_8));
        final String signature = Base64.getEncoder().encodeToString("signature".getBytes(StandardCharsets.UTF_8));
        final String token = header + "." + payload + "." + signature;
        invalidFakeToken.setToken(token);
        invalidTokenRepository.save(invalidFakeToken);

        // WHEN
        final JobExecution jobExecution = jobLauncherTestUtils.launchJob();

        // THEN
        assertThat(jobExecution).isNotNull();
        assertThat(jobExecution.getExitStatus()).isNotNull();
        assertThat(jobExecution.getExitStatus().getExitCode()).isEqualTo(ExitStatus.COMPLETED.getExitCode());
        assertThat(invalidTokenRepository.findInvalidTokenByToken(token)).isNull();
    }

    @Test
    public void purgeInvalidTokenTest() throws Exception {
        // GIVEN
        final InvalidToken invalidFakeToken = new InvalidToken();
        invalidTokenRepository.save(invalidFakeToken);

        // WHEN
        final JobExecution jobExecution = jobLauncherTestUtils.launchJob();

        // THEN
        assertThat(jobExecution).isNotNull();
        assertThat(jobExecution.getExitStatus()).isNotNull();
        assertThat(jobExecution.getExitStatus().getExitCode()).isEqualTo(ExitStatus.COMPLETED.getExitCode());
        assertThat(invalidTokenRepository.findAll()).isEmpty();
    }

    @Test
    public void purgeTokenWithNoExpirationTest() throws Exception {
        // GIVEN
        final InvalidToken invalidAlteredToken = new InvalidToken();
        final String token = alterToken();
        invalidAlteredToken.setToken(token);
        invalidTokenRepository.save(invalidAlteredToken);

        // WHEN
        final JobExecution jobExecution = jobLauncherTestUtils.launchJob();

        // THEN
        assertThat(jobExecution).isNotNull();
        assertThat(jobExecution.getExitStatus()).isNotNull();
        assertThat(jobExecution.getExitStatus().getExitCode()).isEqualTo(ExitStatus.COMPLETED.getExitCode());
        assertThat(invalidTokenRepository.findInvalidTokenByToken(token)).isNull();
    }

    private String alterToken() throws IOException {
        final String[] jwtArray = TestHelper.TOKEN.split("\\.");
        final String payload = jwtArray[1];
        final Claims claims = objectMapper.readValue(Base64.getDecoder().decode(payload.getBytes()), DefaultClaims.class);
        claims.setExpiration(null);
        final String alteredPayload = Base64.getEncoder().encodeToString(objectMapper.writeValueAsBytes(claims));

        return jwtArray[0] + "." + alteredPayload + "." + jwtArray[2];
    }

    @Test
    public void purgeExpiredTokenTest() throws Exception {
        // GIVEN
        final InvalidToken invalidExpiredToken = new InvalidToken();
        invalidExpiredToken.setToken(TestHelper.TOKEN_EXPIRED);
        invalidTokenRepository.save(invalidExpiredToken);

        // WHEN
        final JobExecution jobExecution = jobLauncherTestUtils.launchJob();

        // THEN
        assertThat(jobExecution).isNotNull();
        assertThat(jobExecution.getExitStatus()).isNotNull();
        assertThat(jobExecution.getExitStatus().getExitCode()).isEqualTo(ExitStatus.COMPLETED.getExitCode());
        assertThat(invalidTokenRepository.findInvalidTokenByToken(TestHelper.TOKEN_EXPIRED)).isNull();
    }

    @Test
    public void purgeTokenValidTest() throws Exception {
        // GIVEN
        final InvalidToken invalidToken = new InvalidToken();
        invalidToken.setToken(TestHelper.TOKEN);
        invalidTokenRepository.save(invalidToken);

        // WHEN
        final JobExecution jobExecution = jobLauncherTestUtils.launchJob();

        // THEN
        assertThat(jobExecution).isNotNull();
        assertThat(jobExecution.getExitStatus()).isNotNull();
        assertThat(jobExecution.getExitStatus().getExitCode()).isEqualTo(ExitStatus.COMPLETED.getExitCode());
        assertThat(invalidTokenRepository.findInvalidTokenByToken(TestHelper.TOKEN))
                .isNotNull()
                .extracting(InvalidToken::getToken)
                .isEqualTo(TestHelper.TOKEN);
    }
}
