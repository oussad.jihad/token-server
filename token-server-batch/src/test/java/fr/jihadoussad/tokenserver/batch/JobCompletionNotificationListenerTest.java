package fr.jihadoussad.tokenserver.batch;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;

@ExtendWith(MockitoExtension.class)
public class JobCompletionNotificationListenerTest {

    @InjectMocks
    private JobCompletionNotificationListener jobCompletionNotificationListener;

    @Test
    public void notifyWhenJobFailedTest() {
        final JobExecution jobExecution = new JobExecution(1L);
        jobExecution.setExitStatus(ExitStatus.FAILED);

        jobCompletionNotificationListener.afterJob(jobExecution);
    }
}
