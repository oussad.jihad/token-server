package fr.jihadoussad.tokenserver.client;

import fr.jihadoussad.tokenserver.contract.input.GenerateTokenInput;
import fr.jihadoussad.tokenserver.contract.input.Token;
import fr.jihadoussad.tokenserver.contract.input.VerifyTokenInput;
import io.jsonwebtoken.Claims;
import org.springframework.http.ResponseEntity;

/**
 * Token Client
 */
public interface TokenClient {

    /**
     * Call client server to generate a token
     * @param input the generate token input
     * @return the string token
     */
    ResponseEntity<String> generateToken(final GenerateTokenInput input);

    /**
     * Call client server to extract payload
     * @param jwt the token
     * @return the claims payload
     */
    ResponseEntity<Claims> extractPayload(final Token jwt);

    /**
     * Call client server to verify if token valid
     * @param input the verify token input
     * @return the claims payload extracted
     */
    ResponseEntity<Claims> verifyToken(final VerifyTokenInput input);

    /**
     * Call client server to invalidate token
     * @param jwt the token
     */
    ResponseEntity<Void> invalidateToken(final Token jwt);
}
