package fr.jihadoussad.tokenserver.client;

import fr.jihadoussad.tokenserver.client.context.TokenClientContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TokenClientConfig {

    private final Logger logger = LoggerFactory.getLogger(TokenClientConfig.class);

    @Bean
    TokenClient tokenClient(final ApplicationContext context, final TokenClientContext tokenClientContext) {
        logger.debug("Set application context with following mail client provider: {}", tokenClientContext.getProvider());
        return (TokenClient) context.getBean(tokenClientContext.getProvider());
    }
}
