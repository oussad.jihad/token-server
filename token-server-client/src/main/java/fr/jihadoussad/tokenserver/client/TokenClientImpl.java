package fr.jihadoussad.tokenserver.client;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import fr.jihadoussad.tokenserver.client.context.TokenClientContext;
import fr.jihadoussad.tokenserver.contract.input.Token;
import fr.jihadoussad.tokenserver.contract.input.GenerateTokenInput;
import fr.jihadoussad.tokenserver.contract.input.VerifyTokenInput;
import fr.jihadoussad.tokenserver.jackson.ClaimsDeserializer;
import fr.jihadoussad.tools.api.rest.RestClient;
import fr.jihadoussad.tools.api.rest.RestClientBuilder;
import io.jsonwebtoken.Claims;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;

@Component("token-client-prod")
public class TokenClientImpl implements TokenClient {

    private final RestClient restClient;

    private final String tokenServerUrl;

    private TokenClientImpl(final TokenClientContext tokenClientContext) {
        this.restClient = RestClientBuilder.getInstance()
                .withRestTemplate(restTemplate())
                .build();
        this.tokenServerUrl = tokenClientContext.getUrl();
    }

    /**
     * Build specific token rest template
     * @return the rest template builder
     */
    private RestTemplate restTemplate() {
        final RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
        final MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
        mappingJackson2HttpMessageConverter.setObjectMapper(buildObjectMapper());

        return restTemplateBuilder.messageConverters(new StringHttpMessageConverter(StandardCharsets.UTF_8), mappingJackson2HttpMessageConverter).build();
    }

    /**
     * Build module with claims deserializer
     * @return the specific module
     */
    private Module buildModule() {
        final SimpleModule module = new SimpleModule();
        module.addDeserializer(Claims.class, new ClaimsDeserializer());
        return module;
    }

    /**
     * Build specific object mapper
     * @return the token object mapper
     */
    private ObjectMapper buildObjectMapper() {
        return new Jackson2ObjectMapperBuilder()
                .serializationInclusion(JsonInclude.Include.NON_NULL)
                .modules(buildModule())
                .build();
    }

    @Override
    public ResponseEntity<String> generateToken(final GenerateTokenInput input) {
        return restClient.getResponseEntity(tokenServerUrl + "/generate", HttpMethod.POST, input, new ParameterizedTypeReference<String>() {});
    }

    @Override
    public ResponseEntity<Claims> extractPayload(final Token jwt) {
        return restClient.getResponseEntity(tokenServerUrl + "/payload", HttpMethod.POST, jwt, new ParameterizedTypeReference<Claims>() {});
    }

    @Override
    public ResponseEntity<Claims> verifyToken(final VerifyTokenInput input) {
        return restClient.getResponseEntity(tokenServerUrl + "/verify", HttpMethod.POST, input, new ParameterizedTypeReference<Claims>() {});
    }

    @Override
    public ResponseEntity<Void> invalidateToken(final Token jwt) {
        return restClient.getResponseEntity(tokenServerUrl + "/invalidate", HttpMethod.POST, jwt, new ParameterizedTypeReference<Void>() {});
    }
}
