package fr.jihadoussad.tokenserver.client;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import fr.jihadoussad.tokenserver.client.context.TokenClientContext;
import fr.jihadoussad.tokenserver.contract.exceptions.ContractValidationException;
import fr.jihadoussad.tokenserver.contract.input.GenerateTokenInput;
import fr.jihadoussad.tokenserver.contract.input.Token;
import fr.jihadoussad.tokenserver.contract.input.VerifyTokenInput;
import fr.jihadoussad.tokenserver.jackson.ClaimsDeserializer;
import fr.jihadoussad.tools.api.response.ErrorOutput;
import io.jsonwebtoken.Claims;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.ZonedDateTime;
import java.util.Map;
import java.util.Optional;

import static fr.jihadoussad.tools.api.response.ResponseCode.*;

@Component("token-client-mock")
public class TokenClientMock implements TokenClient {

    private final Logger logger = LoggerFactory.getLogger(TokenClientMock.class);

    private final TokenClientContext context;
    private final ObjectMapper objectMapper;

    public TokenClientMock(final TokenClientContext context) {
        this.objectMapper = buildObjectMapper();
        this.context = context;
    }

    /**
     * Build module with claims deserializer
     * @return the specific module
     */
    private Module buildModule() {
        final SimpleModule module = new SimpleModule();
        module.addDeserializer(Claims.class, new ClaimsDeserializer());
        return module;
    }

    /**
     * Build specific object mapper
     * @return the token object mapper
     */
    private ObjectMapper buildObjectMapper() {
        return new Jackson2ObjectMapperBuilder()
                .serializationInclusion(JsonInclude.Include.NON_NULL)
                .modules(buildModule())
                .build();
    }


    @Override
    public ResponseEntity<String> generateToken(final GenerateTokenInput input) {
        try {
            // Validate input
            logger.debug("Validate generate token input: {}", input);
            input.validate();

            // Compute with json file
            final String token = Optional.ofNullable(extract(context.getMockProperties().getGenerateResponse(), new TypeReference<Map<String, Map<String, Map<String, String>>>>() {}))
                    .map(issuers -> issuers.get(input.getIssuer()))
                    .map(subjects -> subjects.get(input.getSubject()))
                    .map(tokens -> tokens.get(input.getSecretKey()))
                    .orElse(context.getMockProperties().getDefaultToken());

            return ResponseEntity.ok(token);
        } catch (final ContractValidationException exception) {
            logger.info(exception.toString());
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "", handleError(exception.getMessage(), exception.getCode()), null);
        }
    }

    @Override
    public ResponseEntity<Claims> extractPayload(final Token jwt) {
        try {
            // Validate input
            logger.debug("Validate token input: {}", jwt);
            jwt.validate();

            // Build claims with property file
            final Claims claims = Optional.ofNullable(extract(context.getMockProperties().getExtractResponse(), new TypeReference<Map<String, Claims>>() {}))
                    .map(tokens -> tokens.get(jwt.getToken()))
                    .orElse(null);

            return claims != null ? ResponseEntity.ok(claims) : ResponseEntity.ok().build();
        } catch (ContractValidationException exception) {
            if (INVALID_TOKEN.code.equals(exception.getCode())) {
                logger.warn("The token cannot be read as json web token... Returning null payload");
                return ResponseEntity.ok().build();
            } else {
                logger.info(exception.toString());
                throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "", handleError(exception.getMessage(), exception.getCode()), null);
            }
        }
    }

    @Override
    public ResponseEntity<Claims> verifyToken(final VerifyTokenInput input) {
        try {
            // Validate input
            logger.debug("Validate token input to verify: {}", input);
            input.validate();

            // Build claims with property file
            final Optional<Claims> claims = Optional.ofNullable(extract(context.getMockProperties().getVerifyResponse(), new TypeReference<Map<String, Map<String, Claims>>>() {}))
                    .map(tokens -> tokens.get(input.getSecretKey()))
                    .map(payloads -> payloads.get(input.getToken()))
                    .filter(payload -> payload.getSubject() != null && payload.getIssuer() != null);

            // Verify mandatory fields
            if (claims.isEmpty()) {
                logger.warn("No claims with issuer and subject token extracted");
                throw new HttpClientErrorException(HttpStatus.FORBIDDEN, "", handleError(INVALID_TOKEN.message, INVALID_TOKEN.code), null);
            }

            if (Optional.ofNullable(claims.get().getExpiration()).stream().anyMatch(date -> date.toInstant().isBefore(ZonedDateTime.now().toInstant()))) {
                logger.info("The previous expiration");
                throw new HttpClientErrorException(HttpStatus.FORBIDDEN, "", handleError(EXPIRED_TOKEN.message, EXPIRED_TOKEN.code), null);
            }

            // Return
            return ResponseEntity.ok(claims.get());
        } catch (ContractValidationException exception) {
            logger.info(exception.toString());
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "", handleError(exception.getMessage(), exception.getCode()), null);
        }
    }

    @Override
    public ResponseEntity<Void> invalidateToken(final Token jwt) {
        try {
            // Validate input
            logger.debug("Validate token input: {}", jwt);
            jwt.validate();

            return ResponseEntity.ok().build();
        } catch (ContractValidationException exception) {
            if (INVALID_TOKEN.code.equals(exception.getCode())) {
                logger.info(TOKEN_ALREADY_INVALID.message);
                return ResponseEntity.ok().build();
            } else  {
                logger.debug(exception.toString());
                throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "", handleError(exception.getMessage(), exception.getCode()), null);
            }
        }
    }

    private byte[] handleError(final String message, final String code) {
        try {
            final ErrorOutput errorOutput = new ErrorOutput();
            errorOutput.setCode(code);
            errorOutput.setMessage(message);
            return objectMapper.writeValueAsString(errorOutput).getBytes(StandardCharsets.UTF_8);
        } catch (final JsonProcessingException e) {
            return new byte[0];
        }
    }

    private <T> T extract(final String path, final TypeReference<T> typeReference) {
        try {
            return objectMapper.readValue(new File(path), typeReference);
        } catch (IOException exception) {
            logger.warn(exception.toString());
            return null;
        }
    }
}
