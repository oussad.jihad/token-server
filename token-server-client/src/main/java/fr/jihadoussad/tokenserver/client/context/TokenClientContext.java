package fr.jihadoussad.tokenserver.client.context;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "token-client")
public class TokenClientContext {

    private String provider;

    private String url;

    private final MockProperties mockProperties;

    public TokenClientContext() {
        mockProperties = new MockProperties();
        this.provider = "token-client-prod";
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public MockProperties getMockProperties() {
        return mockProperties;
    }

    public static class MockProperties {

        private static final String RESOURCE_PATH = "src/test/resources/";

        private String defaultToken;

        private String generateResponse;

        private String extractResponse;

        private String verifyResponse;

        public MockProperties() {
            this.defaultToken = "defaultToken";
            this.generateResponse = RESOURCE_PATH + "generateResponse.json";
            this.extractResponse = RESOURCE_PATH + "extractResponse.json";
            this.verifyResponse = RESOURCE_PATH + "verifyResponse.json";
        }

        public String getDefaultToken() {
            return defaultToken;
        }

        public void setDefaultToken(String defaultToken) {
            this.defaultToken = defaultToken;
        }

        public String getGenerateResponse() {
            return generateResponse;
        }

        public void setGenerateResponse(String generateResponse) {
            this.generateResponse = generateResponse;
        }

        public String getExtractResponse() {
            return extractResponse;
        }

        public void setExtractResponse(String extractResponse) {
            this.extractResponse = extractResponse;
        }

        public String getVerifyResponse() {
            return verifyResponse;
        }

        public void setVerifyResponse(String verifyResponse) {
            this.verifyResponse = verifyResponse;
        }
    }
}
