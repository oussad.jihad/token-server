package fr.jihadoussad.tokenserver.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.jihadoussad.tokenserver.contract.input.Token;
import fr.jihadoussad.tokenserver.contract.input.GenerateTokenInput;
import fr.jihadoussad.tokenserver.contract.input.VerifyTokenInput;
import fr.jihadoussad.tokenserver.core.entities.InvalidToken;
import fr.jihadoussad.tokenserver.core.repositories.InvalidTokenRepository;
import fr.jihadoussad.tokenserver.test.TestHelper;
import fr.jihadoussad.tools.api.response.ErrorOutput;
import io.jsonwebtoken.Claims;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.HttpClientErrorException;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;
import java.util.Map;

import static fr.jihadoussad.tools.api.response.ResponseCode.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class TokenClientImplIntegrationTest {

    @Autowired
    private InvalidTokenRepository invalidTokenRepository;

    @Autowired
    private TokenClient tokenClient;

    private static final ObjectMapper OBJECT_MAPPER = Jackson2ObjectMapperBuilder.json().build();

    @AfterEach
    public void cleanUp() {
        invalidTokenRepository.deleteAll();
        invalidTokenRepository.flush();
    }

    @Test
    public void generateTokenWhenMissingMandatoryFieldTest() throws JsonProcessingException {
        final GenerateTokenInput input = new GenerateTokenInput();
        final HttpClientErrorException response = assertThrows(HttpClientErrorException.class, () -> tokenClient.generateToken(input));

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getResponseBodyAsString()).isNotNull();

        final ErrorOutput output = OBJECT_MAPPER.readValue(response.getResponseBodyAsString(), ErrorOutput.class);
        assertThat(output.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
    }

    @Test
    public void generateTokenWhenSecretKeyNotEncodedTest() throws JsonProcessingException {
        final GenerateTokenInput input = new GenerateTokenInput();
        input.setSecretKey("badSecretKey");
        final HttpClientErrorException response = assertThrows(HttpClientErrorException.class, () -> tokenClient.generateToken(input));

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getResponseBodyAsString()).isNotNull();

        final ErrorOutput output = OBJECT_MAPPER.readValue(response.getResponseBodyAsString(), ErrorOutput.class);
        assertThat(output.getCode()).isEqualTo(INVALID_SECRET_KEY.code);
    }

    @Test
    public void generateTokenWhenSecretKeySizeLessThan256BitsTest() throws JsonProcessingException {
        final GenerateTokenInput input = new GenerateTokenInput();
        input.setSecretKey(Base64.getEncoder().encodeToString("badSecretKey".getBytes(StandardCharsets.UTF_8)));
        final HttpClientErrorException response = assertThrows(HttpClientErrorException.class, () -> tokenClient.generateToken(input));

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getResponseBodyAsString()).isNotNull();

        final ErrorOutput output = OBJECT_MAPPER.readValue(response.getResponseBodyAsString(), ErrorOutput.class);
        assertThat(output.getCode()).isEqualTo(INVALID_SECRET_KEY.code);
    }

    @Test
    public void extractPayloadWhenTokenNullTest() throws JsonProcessingException {
        final Token input = new Token();
        final HttpClientErrorException response = assertThrows(HttpClientErrorException.class, () -> tokenClient.extractPayload(input));

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getResponseBodyAsString()).isNotNull();

        final ErrorOutput output = OBJECT_MAPPER.readValue(response.getResponseBodyAsString(), ErrorOutput.class);
        assertThat(output.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
    }

    @Test
    public void extractPayloadWhenTokenInvalidTest() {
        final Token token = new Token();
        token.setToken("token");

        final ResponseEntity<Claims> response = tokenClient.extractPayload(token);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNull();
    }

    @Test
    public void extractPayloadWhenPayloadInvalidTest() {
        final Token input = new Token();
        input.setToken("header.fakePayload.signature");

        final ResponseEntity<Claims> response = tokenClient.extractPayload(input);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNull();
    }

    @Test
    public void extractPayloadTest() {
        final Token input = new Token();
        input.setToken(TestHelper.TOKEN);

        final ResponseEntity<Claims> response = tokenClient.extractPayload(input);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getIssuer()).isEqualTo(TestHelper.ISSUER);
        assertThat(response.getBody().getSubject()).isEqualTo(TestHelper.SUBJECT);
    }

    @Test
    public void verifyTokenWhenMissingMandatoryFieldTest() throws JsonProcessingException {
        final VerifyTokenInput verifyTokenInput = new VerifyTokenInput();
        final HttpClientErrorException response = assertThrows(HttpClientErrorException.class, () -> tokenClient.verifyToken(verifyTokenInput));

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getResponseBodyAsString()).isNotNull();

        final ErrorOutput output = OBJECT_MAPPER.readValue(response.getResponseBodyAsString(), ErrorOutput.class);
        assertThat(output.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
    }

    @Test
    public void verifyTokenWhenSecretKeyNotEncodedTest() throws JsonProcessingException {
        final VerifyTokenInput verifyTokenInput = new VerifyTokenInput();
        verifyTokenInput.setToken("token");
        verifyTokenInput.setSecretKey("badSecretKey");
        final HttpClientErrorException response = assertThrows(HttpClientErrorException.class, () -> tokenClient.verifyToken(verifyTokenInput));

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getResponseBodyAsString()).isNotNull();

        final ErrorOutput output = OBJECT_MAPPER.readValue(response.getResponseBodyAsString(), ErrorOutput.class);
        assertThat(output.getCode()).isEqualTo(INVALID_SECRET_KEY.code);
    }

    @Test
    public void verifyTokenWhenSecretKeySizeLessThan256BitsTest() throws JsonProcessingException {
        final VerifyTokenInput verifyTokenInput = new VerifyTokenInput();
        verifyTokenInput.setToken("token");
        verifyTokenInput.setSecretKey(Base64.getEncoder().encodeToString("badSecretKey".getBytes(StandardCharsets.UTF_8)));
        final HttpClientErrorException response = assertThrows(HttpClientErrorException.class, () -> tokenClient.verifyToken(verifyTokenInput));

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getResponseBodyAsString()).isNotNull();

        final ErrorOutput output = OBJECT_MAPPER.readValue(response.getResponseBodyAsString(), ErrorOutput.class);
        assertThat(output.getCode()).isEqualTo(INVALID_SECRET_KEY.code);
    }

    @Test
    public void verifyTokenWhenTokenInvalidTest() throws JsonProcessingException {
        final VerifyTokenInput verifyTokenInput = new VerifyTokenInput();
        verifyTokenInput.setSecretKey(TestHelper.SECRET_KEY);
        verifyTokenInput.setToken("token");
        final HttpClientErrorException response = assertThrows(HttpClientErrorException.class, () -> tokenClient.verifyToken(verifyTokenInput));

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
        assertThat(response.getResponseBodyAsString()).isNotNull();

        final ErrorOutput output = OBJECT_MAPPER.readValue(response.getResponseBodyAsString(), ErrorOutput.class);
        assertThat(output.getCode()).isEqualTo(INVALID_TOKEN.code);
    }

    @Test
    public void verifyTokenWhenTokenExpiredTest() throws JsonProcessingException {
        final VerifyTokenInput verifyTokenInput = new VerifyTokenInput();
        verifyTokenInput.setSecretKey(TestHelper.SECRET_KEY);
        verifyTokenInput.setToken(TestHelper.TOKEN_EXPIRED);
        final HttpClientErrorException response = assertThrows(HttpClientErrorException.class, () -> tokenClient.verifyToken(verifyTokenInput));

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
        assertThat(response.getResponseBodyAsString()).isNotNull();

        final ErrorOutput output = OBJECT_MAPPER.readValue(response.getResponseBodyAsString(), ErrorOutput.class);
        assertThat(output.getCode()).isEqualTo(EXPIRED_TOKEN.code);
    }

    @Test
    public void invalidateTokenWhenMissingMandatoryField() throws JsonProcessingException {
        final Token jwt = new Token();
        final HttpClientErrorException response = assertThrows(HttpClientErrorException.class, () -> tokenClient.invalidateToken(jwt));

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getResponseBodyAsString()).isNotNull();

        final ErrorOutput output = OBJECT_MAPPER.readValue(response.getResponseBodyAsString(), ErrorOutput.class);
        assertThat(output.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
    }

    @Test
    public void invalidateTokenWhenInvalidField() {
        final Token jwt = new Token();
        jwt.setToken("token");
        tokenClient.invalidateToken(jwt);
    }

    @Test
    public void invalidateTokenWhenTokenAlreadyInvalidField() {
        final InvalidToken invalidToken= new InvalidToken();
        invalidToken.setToken(TestHelper.TOKEN);
        invalidTokenRepository.saveAndFlush(invalidToken);

        final Token jwt = new Token();
        jwt.setToken(TestHelper.TOKEN);
        tokenClient.invalidateToken(jwt);
    }

    @Test
    public void generateVerifyAndInvalidateTokenTest() {
        // Generate a token

        final GenerateTokenInput input = new GenerateTokenInput();
        input.setSecretKey(TestHelper.SECRET_KEY);
        input.setExpiration(1000000L);
        input.setIssuer("issuer");
        input.setSubject("subject");
        input.setClaims(Map.of("privileges", List.of("ADMIN", "USER")));

        final ResponseEntity<String> response = tokenClient.generateToken(input);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();

        final String jwt = response.getBody();

        // Verify token validity

        final VerifyTokenInput verifyTokenInput = new VerifyTokenInput();
        verifyTokenInput.setToken(jwt);
        verifyTokenInput.setSecretKey(TestHelper.SECRET_KEY);

        final ResponseEntity<Claims> verifyResponse = tokenClient.verifyToken(verifyTokenInput);

        assertThat(verifyResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(verifyResponse.getBody()).isNotNull();

        // Invalidate the token

        final Token token = new Token();
        token.setToken(jwt);

        final ResponseEntity<Void> invalidateResponse = tokenClient.invalidateToken(token);

        assertThat(invalidateResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
    }
}
