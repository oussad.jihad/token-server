package fr.jihadoussad.tokenserver.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.jihadoussad.tokenserver.client.context.TokenClientContext;
import fr.jihadoussad.tokenserver.contract.input.GenerateTokenInput;
import fr.jihadoussad.tokenserver.contract.input.Token;
import fr.jihadoussad.tokenserver.contract.input.VerifyTokenInput;
import fr.jihadoussad.tokenserver.test.TestHelper;
import fr.jihadoussad.tools.api.response.ErrorOutput;
import io.jsonwebtoken.Claims;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.web.client.HttpClientErrorException;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;
import java.util.Map;

import static fr.jihadoussad.tools.api.response.ResponseCode.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, properties = {"token-client.provider=token-client-mock"})
class TokenClientMockIntegrationTest {

    @Autowired
    private TokenClient tokenClient;

    @Autowired
    private TokenClientContext context;

    private static final ObjectMapper OBJECT_MAPPER = Jackson2ObjectMapperBuilder.json().build();

    @Test
    public void generateTokenWhenMissingMandatoryFieldTest() throws JsonProcessingException {
        final GenerateTokenInput input = new GenerateTokenInput();
        final HttpClientErrorException response = assertThrows(HttpClientErrorException.class, () -> tokenClient.generateToken(input));

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getResponseBodyAsString()).isNotNull();

        final ErrorOutput output = OBJECT_MAPPER.readValue(response.getResponseBodyAsString(), ErrorOutput.class);
        assertThat(output.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
    }

    @Test
    public void generateTokenWhenSecretKeyNotEncodedTest() throws JsonProcessingException {
        final GenerateTokenInput input = new GenerateTokenInput();
        input.setSecretKey("badSecretKey");
        final HttpClientErrorException response = assertThrows(HttpClientErrorException.class, () -> tokenClient.generateToken(input));

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getResponseBodyAsString()).isNotNull();

        final ErrorOutput output = OBJECT_MAPPER.readValue(response.getResponseBodyAsString(), ErrorOutput.class);
        assertThat(output.getCode()).isEqualTo(INVALID_SECRET_KEY.code);
    }

    @Test
    public void generateTokenWhenSecretKeySizeLessThan256BitsTest() throws JsonProcessingException {
        final GenerateTokenInput input = new GenerateTokenInput();
        input.setSecretKey(Base64.getEncoder().encodeToString("badSecretKey".getBytes(StandardCharsets.UTF_8)));
        final HttpClientErrorException response = assertThrows(HttpClientErrorException.class, () -> tokenClient.generateToken(input));

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getResponseBodyAsString()).isNotNull();

        final ErrorOutput output = OBJECT_MAPPER.readValue(response.getResponseBodyAsString(), ErrorOutput.class);
        assertThat(output.getCode()).isEqualTo(INVALID_SECRET_KEY.code);
    }

    @Test
    public void extractPayloadWhenTokenNullTest() throws JsonProcessingException {
        final Token input = new Token();
        final HttpClientErrorException response = assertThrows(HttpClientErrorException.class, () -> tokenClient.extractPayload(input));

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getResponseBodyAsString()).isNotNull();

        final ErrorOutput output = OBJECT_MAPPER.readValue(response.getResponseBodyAsString(), ErrorOutput.class);
        assertThat(output.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
    }

    @Test
    public void extractPayloadWhenTokenInvalidTest() {
        final Token token = new Token();
        token.setToken("token");

        final ResponseEntity<Claims> response = tokenClient.extractPayload(token);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNull();
    }

    @Test
    public void extractPayloadWhenPayloadInvalidTest() {
        final Token input = new Token();
        input.setToken("header.fakePayload.signature");

        final ResponseEntity<Claims> response = tokenClient.extractPayload(input);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNull();
    }

    @Test
    public void extractPayloadWhenClaimsPathNotFoundTest() {
        context.getMockProperties().setExtractResponse("badResponse.json");

        final Token input = new Token();
        input.setToken(TestHelper.TOKEN_CLAIMS_INVALID);

        final ResponseEntity<Claims> response = tokenClient.extractPayload(input);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNull();

        context.getMockProperties().setExtractResponse("src/test/resources/extractResponse.json");
    }

    @Test
    public void extractPayloadTest() {
        final Token input = new Token();
        input.setToken(TestHelper.TOKEN);

        final ResponseEntity<Claims> response = tokenClient.extractPayload(input);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getIssuer()).isEqualTo(TestHelper.ISSUER);
        assertThat(response.getBody().getSubject()).isEqualTo(TestHelper.SUBJECT);
    }

    @Test
    public void verifyTokenWhenMissingMandatoryFieldTest() throws JsonProcessingException {
        final VerifyTokenInput verifyTokenInput = new VerifyTokenInput();
        final HttpClientErrorException response = assertThrows(HttpClientErrorException.class, () -> tokenClient.verifyToken(verifyTokenInput));

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getResponseBodyAsString()).isNotNull();

        final ErrorOutput output = OBJECT_MAPPER.readValue(response.getResponseBodyAsString(), ErrorOutput.class);
        assertThat(output.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
    }

    @Test
    public void verifyTokenWhenSecretKeyNotEncodedTest() throws JsonProcessingException {
        final VerifyTokenInput verifyTokenInput = new VerifyTokenInput();
        verifyTokenInput.setToken("token");
        verifyTokenInput.setSecretKey("badSecretKey");
        final HttpClientErrorException response = assertThrows(HttpClientErrorException.class, () -> tokenClient.verifyToken(verifyTokenInput));

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getResponseBodyAsString()).isNotNull();

        final ErrorOutput output = OBJECT_MAPPER.readValue(response.getResponseBodyAsString(), ErrorOutput.class);
        assertThat(output.getCode()).isEqualTo(INVALID_SECRET_KEY.code);
    }

    @Test
    public void verifyTokenWhenSecretKeySizeLessThan256BitsTest() throws JsonProcessingException {
        final VerifyTokenInput verifyTokenInput = new VerifyTokenInput();
        verifyTokenInput.setToken("token");
        verifyTokenInput.setSecretKey(Base64.getEncoder().encodeToString("badSecretKey".getBytes(StandardCharsets.UTF_8)));
        final HttpClientErrorException response = assertThrows(HttpClientErrorException.class, () -> tokenClient.verifyToken(verifyTokenInput));

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getResponseBodyAsString()).isNotNull();

        final ErrorOutput output = OBJECT_MAPPER.readValue(response.getResponseBodyAsString(), ErrorOutput.class);
        assertThat(output.getCode()).isEqualTo(INVALID_SECRET_KEY.code);
    }

    @Test
    public void verifyTokenWhenTokenInvalidTest() throws JsonProcessingException {
        final VerifyTokenInput verifyTokenInput = new VerifyTokenInput();
        verifyTokenInput.setSecretKey(TestHelper.SECRET_KEY);
        verifyTokenInput.setToken("token");
        final HttpClientErrorException response = assertThrows(HttpClientErrorException.class, () -> tokenClient.verifyToken(verifyTokenInput));

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
        assertThat(response.getResponseBodyAsString()).isNotNull();

        final ErrorOutput output = OBJECT_MAPPER.readValue(response.getResponseBodyAsString(), ErrorOutput.class);
        assertThat(output.getCode()).isEqualTo(INVALID_TOKEN.code);
    }

    @Test
    public void verifyTokenWhenTokenExpiredTest() throws JsonProcessingException {
        final VerifyTokenInput verifyTokenInput = new VerifyTokenInput();
        verifyTokenInput.setSecretKey(TestHelper.SECRET_KEY);
        verifyTokenInput.setToken(TestHelper.TOKEN_EXPIRED);
        final HttpClientErrorException response = assertThrows(HttpClientErrorException.class, () -> tokenClient.verifyToken(verifyTokenInput));

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
        assertThat(response.getResponseBodyAsString()).isNotNull();

        final ErrorOutput output = OBJECT_MAPPER.readValue(response.getResponseBodyAsString(), ErrorOutput.class);
        assertThat(output.getCode()).isEqualTo(EXPIRED_TOKEN.code);
    }

    @Test
    public void verifyTokenWhenClaimsPathNotFoundTest() throws JsonProcessingException {
        context.getMockProperties().setVerifyResponse("badResponse.json");

        final VerifyTokenInput verifyTokenInput = new VerifyTokenInput();
        verifyTokenInput.setSecretKey(TestHelper.SECRET_KEY);
        verifyTokenInput.setToken(TestHelper.TOKEN_CLAIMS_INVALID);
        final HttpClientErrorException response = assertThrows(HttpClientErrorException.class, () -> tokenClient.verifyToken(verifyTokenInput));

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
        assertThat(response.getResponseBodyAsString()).isNotNull();

        final ErrorOutput output = OBJECT_MAPPER.readValue(response.getResponseBodyAsString(), ErrorOutput.class);
        assertThat(output.getCode()).isEqualTo(INVALID_TOKEN.code);

    }

    @Test
    public void verifyTokenWhenNoIssuerFoundTest() throws JsonProcessingException {
        final VerifyTokenInput verifyTokenInput = new VerifyTokenInput();
        verifyTokenInput.setSecretKey(TestHelper.SECRET_KEY);
        verifyTokenInput.setToken("eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJiZDI0MTE0NDFhZDB0YjU4OGLiZjIwOGRkNTliMzg0NSIsImlzcyI6Imlzc3VlciIsImlhdCI6MTYxMzUwMDQxNSwibmJmIjoxNjEzNTAwNDE1LCJleHAiOjMzMTcwNDI2NDE1LCJzdWIiOiJzdWJqZWN0IiwiY2RhaW0iOiJjbGFpbSJ9.5OWa4Emd_SVfqYbUc7jYvOB5ttH4uxEiWP1KoVIKISs");
        final HttpClientErrorException response = assertThrows(HttpClientErrorException.class, () -> tokenClient.verifyToken(verifyTokenInput));

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
        assertThat(response.getResponseBodyAsString()).isNotNull();

        final ErrorOutput output = OBJECT_MAPPER.readValue(response.getResponseBodyAsString(), ErrorOutput.class);
        assertThat(output.getCode()).isEqualTo(INVALID_TOKEN.code);

    }

    @Test
    public void verifyTokenWhenNoSubjectFoundTest() throws JsonProcessingException {
        final VerifyTokenInput verifyTokenInput = new VerifyTokenInput();
        verifyTokenInput.setSecretKey(TestHelper.SECRET_KEY);
        verifyTokenInput.setToken("eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJiZDI0MTE0NDFhZDB0YjU4OGLiZjIwOGRkNTliMzg0NSIsImlzcyI6Imlzc3VlciIsImlhdCI6MTYxMzUwMDQxNSwibmJmIjoxNjEzNTAwNDE1LCJleHAiOjMzMTcwNDI2NDE1LCJzdWIiOiJzdWJqZWN0IiwiY2RhaW0iOiJjbGFpbSJ9.5OWa4Emd_SVfqYbUc7jYvOB5ttH4uxEiWP1KoVIKSuB");
        final HttpClientErrorException response = assertThrows(HttpClientErrorException.class, () -> tokenClient.verifyToken(verifyTokenInput));

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
        assertThat(response.getResponseBodyAsString()).isNotNull();

        final ErrorOutput output = OBJECT_MAPPER.readValue(response.getResponseBodyAsString(), ErrorOutput.class);
        assertThat(output.getCode()).isEqualTo(INVALID_TOKEN.code);

    }

    @Test
    public void invalidateTokenWhenMissingMandatoryField() throws JsonProcessingException {
        final Token jwt = new Token();
        final HttpClientErrorException response = assertThrows(HttpClientErrorException.class, () -> tokenClient.invalidateToken(jwt));

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getResponseBodyAsString()).isNotNull();

        final ErrorOutput output = OBJECT_MAPPER.readValue(response.getResponseBodyAsString(), ErrorOutput.class);
        assertThat(output.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
    }

    @Test
    public void invalidateTokenWhenInvalidField() {
        final Token jwt = new Token();
        jwt.setToken("token");
        tokenClient.invalidateToken(jwt);
    }

    @Test
    public void invalidateTokenWhenTokenAlreadyInvalidField() {
        final Token jwt = new Token();
        jwt.setToken(TestHelper.TOKEN);
        tokenClient.invalidateToken(jwt);
    }

    @Test
    public void generateVerifyAndInvalidateTokenTest() {
        // Generate a token

        final GenerateTokenInput input = new GenerateTokenInput();
        input.setSecretKey(TestHelper.SECRET_KEY);
        input.setExpiration(1000000L);
        input.setIssuer("issuer");
        input.setSubject("subject");
        input.setClaims(Map.of("privileges", List.of("ADMIN", "USER")));

        final ResponseEntity<String> response = tokenClient.generateToken(input);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();

        final String jwt = response.getBody();

        // Verify token validity

        final VerifyTokenInput verifyTokenInput = new VerifyTokenInput();
        verifyTokenInput.setToken(jwt);
        verifyTokenInput.setSecretKey(TestHelper.SECRET_KEY);

        final ResponseEntity<Claims> verifyResponse = tokenClient.verifyToken(verifyTokenInput);

        assertThat(verifyResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(verifyResponse.getBody()).isNotNull();

        // Invalidate the token

        final Token token = new Token();
        token.setToken(jwt);

        final ResponseEntity<Void> invalidateResponse = tokenClient.invalidateToken(token);

        assertThat(invalidateResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
    }
}
