package fr.jihadoussad.tokenserver.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = "fr.jihadoussad.tokenserver.*")
@EntityScan(basePackages = "fr.jihadoussad.tokenserver.core.*")
@EnableJpaRepositories(basePackages = "fr.jihadoussad.tokenserver.core.*")
public class TokenServerApplication {

    public static void main(final String[] args) {
        SpringApplication.run(TokenServerApplication.class);
    }
}
