package fr.jihadoussad.tokenserver.contract;

import fr.jihadoussad.tokenserver.contract.exceptions.ContractValidationException;
import fr.jihadoussad.tokenserver.contract.input.Token;
import fr.jihadoussad.tokenserver.contract.input.GenerateTokenInput;
import fr.jihadoussad.tokenserver.contract.input.VerifyTokenInput;
import io.jsonwebtoken.Claims;

/**
 * Token service
 */
public interface TokenService {

    /**
     * Generate a token
     * @param input the secret key generated + validity time after expiration + subject + claims
     * @return the Json Web Token (header + payload + signature)
     * @throws ContractValidationException if input fields are missing
     */
    String generateToken(final GenerateTokenInput input) throws ContractValidationException;

    /**
     * Extract the token payload
     * @param jwt (header + payload + signature)
     * @return the payload in map form (claims)
     * @throws ContractValidationException if the token is missing
     */
    Claims extractPayload(final Token jwt) throws ContractValidationException;

    /**
     * Verify if the token is valid
     * @param input the Json Web Token (header + payload + signature) + secret key
     * @return payload information about this token (subject, expiration etc...)
     * @throws ContractValidationException if input fields are missing or the token is invalid or expired
     */
    Claims verifyToken(final VerifyTokenInput input) throws ContractValidationException;

    /**
     * Invalidate a token
     * @param jwt (header + payload + signature) + secret key)
     * @return response message if process ok
     * @throws ContractValidationException if the token is already invalid or missing
     */
    void invalidateToken(final Token jwt) throws ContractValidationException;
}
