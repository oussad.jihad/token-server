package fr.jihadoussad.tokenserver.contract;

import fr.jihadoussad.tokenserver.contract.exceptions.ContractValidationException;

/**
 * Validable
 */
public interface Validable {

    void validate() throws ContractValidationException;
}
