package fr.jihadoussad.tokenserver.contract.input;

import fr.jihadoussad.tokenserver.contract.Validable;
import fr.jihadoussad.tokenserver.contract.exceptions.ContractValidationException;
import fr.jihadoussad.tokenserver.contract.validators.SecretKeyValidator;

import java.util.Map;
import java.util.StringJoiner;

import static fr.jihadoussad.tools.api.response.ResponseCode.INVALID_SECRET_KEY;
import static fr.jihadoussad.tools.api.response.ResponseCode.MISSING_MANDATORY_FIELD;

public class GenerateTokenInput  implements Validable {

    private String secretKey;

    private Long expiration;

    private String issuer;

    private String subject;

    private Map<String, Object> claims;

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(final String secretKey) {
        this.secretKey = secretKey;
    }

    public Long getExpiration() {
        return expiration;
    }

    public void setExpiration(final Long expiration) {
        this.expiration = expiration;
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(final String subject) {
        this.subject = subject;
    }

    public Map<String, Object> getClaims() {
        return claims;
    }

    public void setClaims(final Map<String, Object> claims) {
        this.claims = claims;
    }

    @Override
    public void validate() throws ContractValidationException {
        if (secretKey == null) {
            throw new ContractValidationException(MISSING_MANDATORY_FIELD.message + "secret key null", MISSING_MANDATORY_FIELD.code);
        }
        if (!SecretKeyValidator.validate(secretKey)) {
            throw new ContractValidationException(INVALID_SECRET_KEY.message, INVALID_SECRET_KEY.code);
        }
        if (expiration == null) {
            throw new ContractValidationException(MISSING_MANDATORY_FIELD.message + "expiration null", MISSING_MANDATORY_FIELD.code);
        }
        if (issuer == null) {
            throw new ContractValidationException(MISSING_MANDATORY_FIELD.message + "issuer null", MISSING_MANDATORY_FIELD.code);
        }
        if (subject == null) {
            throw new ContractValidationException(MISSING_MANDATORY_FIELD.message + "subject null", MISSING_MANDATORY_FIELD.code);
        }
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", GenerateTokenInput.class.getSimpleName() + "[", "]")
                .add("secretKey='" + secretKey + "'")
                .add("expiration=" + expiration)
                .add("issuer=" + issuer + "'")
                .add("subject='" + subject + "'")
                .add("claims=" + claims)
                .toString();
    }
}
