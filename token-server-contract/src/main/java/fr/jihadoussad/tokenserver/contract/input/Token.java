package fr.jihadoussad.tokenserver.contract.input;

import fr.jihadoussad.tokenserver.contract.Validable;
import fr.jihadoussad.tokenserver.contract.exceptions.ContractValidationException;
import fr.jihadoussad.tokenserver.contract.validators.JWTokenValidator;

import java.util.StringJoiner;

import static fr.jihadoussad.tools.api.response.ResponseCode.*;

public class Token implements Validable {

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public void validate() throws ContractValidationException {
        if (token == null) {
            throw new ContractValidationException(MISSING_MANDATORY_FIELD.message + "token null", MISSING_MANDATORY_FIELD.code);
        }
        if (!JWTokenValidator.validate(token)) {
            throw new ContractValidationException(INVALID_TOKEN.message, INVALID_TOKEN.code);
        }
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Token.class.getSimpleName() + "[", "]")
                .add("token='" + token + "'")
                .toString();
    }
}
