package fr.jihadoussad.tokenserver.contract.input;

import fr.jihadoussad.tokenserver.contract.Validable;
import fr.jihadoussad.tokenserver.contract.exceptions.ContractValidationException;
import fr.jihadoussad.tokenserver.contract.validators.SecretKeyValidator;

import java.util.StringJoiner;

import static fr.jihadoussad.tools.api.response.ResponseCode.INVALID_SECRET_KEY;
import static fr.jihadoussad.tools.api.response.ResponseCode.MISSING_MANDATORY_FIELD;

public class VerifyTokenInput implements Validable {

    private String token;

    private String secretKey;

    public String getToken() {
        return token;
    }

    public void setToken(final String token) {
        this.token = token;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(final String secretKey) {
        this.secretKey = secretKey;
    }

    @Override
    public void validate() throws ContractValidationException {
        if (token == null) {
            throw new ContractValidationException(MISSING_MANDATORY_FIELD.message + "token null", MISSING_MANDATORY_FIELD.code);
        }
        if (secretKey == null) {
            throw new ContractValidationException(MISSING_MANDATORY_FIELD.message + "secret key null", MISSING_MANDATORY_FIELD.code);
        }
        if (!SecretKeyValidator.validate(secretKey)) {
            throw new ContractValidationException(INVALID_SECRET_KEY.message, INVALID_SECRET_KEY.code);
        }
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", VerifyTokenInput.class.getSimpleName() + "[", "]")
                .add("token='" + token + "'")
                .add("secretKey=" + secretKey)
                .toString();
    }
}
