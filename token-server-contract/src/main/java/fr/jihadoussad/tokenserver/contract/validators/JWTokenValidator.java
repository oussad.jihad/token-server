package fr.jihadoussad.tokenserver.contract.validators;

import java.util.Base64;

public class JWTokenValidator {

    private JWTokenValidator() {}

    public static boolean validate(final String jwt) {
        final String[] arrayJwt = jwt.split("\\.");

        if (arrayJwt.length != 3) {
            return false;
        }

        try {
            Base64.getDecoder().decode(arrayJwt[0]);
            Base64.getDecoder().decode(arrayJwt[1]);
            return true;
        } catch (final Exception exception) {
            return false;
        }
    }
}
