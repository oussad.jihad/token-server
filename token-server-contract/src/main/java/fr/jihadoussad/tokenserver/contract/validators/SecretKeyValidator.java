package fr.jihadoussad.tokenserver.contract.validators;

import fr.jihadoussad.tools.security.KeyUtils;

public class SecretKeyValidator {

    private SecretKeyValidator() {}

    public static boolean validate(final String secretKey) {
        try {
            return KeyUtils.stringToKey(secretKey).getEncoded().length * 8 >= 256 ;
        } catch (final Exception exception) {
            return false;
        }
    }
}
