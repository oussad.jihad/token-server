package fr.jihadoussad.tokenserver.contract.input;

import fr.jihadoussad.tokenserver.contract.exceptions.ContractValidationException;
import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Map;

import static fr.jihadoussad.tools.api.response.ResponseCode.INVALID_SECRET_KEY;
import static fr.jihadoussad.tools.api.response.ResponseCode.MISSING_MANDATORY_FIELD;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class GenerateTokenInputTest {

    private static final String SECRET_KEY = "tAC24j0OfEVFeXRH2ubfsrHNNzp/bxEmcaf5yWl108c=";

    @Test
    public void validateWhenMissingSecretKeyMandatoryTest() {
        final GenerateTokenInput input = new GenerateTokenInput();
        final ContractValidationException exception = assertThrows(ContractValidationException.class, input::validate);
        assertThat(exception.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
        assertThat(exception.getMessage()).isEqualTo(MISSING_MANDATORY_FIELD.message + "secret key null");
    }

    @Test
    public void validateWhenSecretKeyNotEncodedTest() {
        final GenerateTokenInput input = new GenerateTokenInput();
        input.setSecretKey("badSecretKey");
        final ContractValidationException exception = assertThrows(ContractValidationException.class, input::validate);
        assertThat(exception.getCode()).isEqualTo(INVALID_SECRET_KEY.code);
        assertThat(exception.getMessage()).isEqualTo(INVALID_SECRET_KEY.message);
    }

    @Test
    public void validateWhenSecretKeySizeLessThan256BitsTest() {
        final GenerateTokenInput input = new GenerateTokenInput();
        input.setSecretKey(Base64.getEncoder().encodeToString("badSecretKey".getBytes(StandardCharsets.UTF_8)));
        final ContractValidationException exception = assertThrows(ContractValidationException.class, input::validate);
        assertThat(exception.getCode()).isEqualTo(INVALID_SECRET_KEY.code);
        assertThat(exception.getMessage()).isEqualTo(INVALID_SECRET_KEY.message);
    }

    @Test
    public void validateWhenMissingExpirationMandatoryFieldTest() {
        final GenerateTokenInput input = new GenerateTokenInput();
        input.setSecretKey(SECRET_KEY);
        final ContractValidationException exception = assertThrows(ContractValidationException.class, input::validate);
        assertThat(exception.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
        assertThat(exception.getMessage()).isEqualTo(MISSING_MANDATORY_FIELD.message + "expiration null");
    }

    @Test
    public void validateWhenMissingIssuerMandatoryFieldTest() {
        final GenerateTokenInput input = new GenerateTokenInput();
        input.setSecretKey(SECRET_KEY);
        input.setExpiration(10000L);
        final ContractValidationException exception = assertThrows(ContractValidationException.class, input::validate);
        assertThat(exception.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
        assertThat(exception.getMessage()).isEqualTo(MISSING_MANDATORY_FIELD.message + "issuer null");
    }

    @Test
    public void validateWhenMissingSubjectMandatoryFieldTest() {
        final GenerateTokenInput input = new GenerateTokenInput();
        input.setSecretKey(SECRET_KEY);
        input.setExpiration(10000L);
        input.setIssuer("issuer");
        final ContractValidationException exception = assertThrows(ContractValidationException.class, input::validate);
        assertThat(exception.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
        assertThat(exception.getMessage()).isEqualTo(MISSING_MANDATORY_FIELD.message + "subject null");
    }

    @Test
    public void validateInput() throws ContractValidationException {
        final GenerateTokenInput input = new GenerateTokenInput();
        input.setSecretKey(SECRET_KEY);
        input.setExpiration(10000L);
        input.setIssuer("issuer");
        input.setSubject("subject");
        input.setClaims(Map.of("claim", "claim"));

        input.validate();
        assertThat(input.toString()).isNotNull();
    }
}
