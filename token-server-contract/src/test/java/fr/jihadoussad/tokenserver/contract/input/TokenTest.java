package fr.jihadoussad.tokenserver.contract.input;

import fr.jihadoussad.tokenserver.contract.exceptions.ContractValidationException;
import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

import static fr.jihadoussad.tools.api.response.ResponseCode.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TokenTest {

    @Test
    public void validateWhenMissingTokenMandatoryTest() {
        final Token input = new Token();
        final ContractValidationException exception = assertThrows(ContractValidationException.class, input::validate);
        assertThat(exception.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
        assertThat(exception.getMessage()).isEqualTo(MISSING_MANDATORY_FIELD.message + "token null");
    }

    @Test
    public void validateWhenTokenAlreadyInvalidTest() {
        final Token input = new Token();
        input.setToken("fakeToken");
        final ContractValidationException exception = assertThrows(ContractValidationException.class, input::validate);
        assertThat(exception.getCode()).isEqualTo(INVALID_TOKEN.code);
        assertThat(exception.getMessage()).isEqualTo(INVALID_TOKEN.message);
    }

    @Test
    public void validateInput() throws ContractValidationException {
        final Token input = new Token();
        final String header = Base64.getEncoder().encodeToString("header".getBytes(StandardCharsets.UTF_8));
        final String payload = Base64.getEncoder().encodeToString("payload".getBytes(StandardCharsets.UTF_8));
        final String signature = Base64.getEncoder().encodeToString("signature".getBytes(StandardCharsets.UTF_8));
        final String token = header + "." + payload + "." + signature;
        input.setToken(token);

        input.validate();
        assertThat(input.toString()).isNotNull();
    }
}
