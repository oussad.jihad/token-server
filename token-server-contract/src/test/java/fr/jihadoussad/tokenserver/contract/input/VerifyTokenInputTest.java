package fr.jihadoussad.tokenserver.contract.input;

import fr.jihadoussad.tokenserver.contract.exceptions.ContractValidationException;
import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

import static fr.jihadoussad.tools.api.response.ResponseCode.INVALID_SECRET_KEY;
import static fr.jihadoussad.tools.api.response.ResponseCode.MISSING_MANDATORY_FIELD;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class VerifyTokenInputTest {

    @Test
    public void validateWhenMissingTokenMandatoryTest() {
        final VerifyTokenInput input = new VerifyTokenInput();
        final ContractValidationException exception = assertThrows(ContractValidationException.class, input::validate);
        assertThat(exception.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
        assertThat(exception.getMessage()).isEqualTo(MISSING_MANDATORY_FIELD.message + "token null");
    }

    @Test
    public void validateWhenMissingSecretKeyMandatoryTest() {
        final VerifyTokenInput input = new VerifyTokenInput();
        input.setToken("token");
        final ContractValidationException exception = assertThrows(ContractValidationException.class, input::validate);
        assertThat(exception.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
        assertThat(exception.getMessage()).isEqualTo(MISSING_MANDATORY_FIELD.message + "secret key null");
    }

    @Test
    public void validateWhenSecretKeyNotEncodedTest() {
        final VerifyTokenInput input = new VerifyTokenInput();
        input.setToken("token");
        input.setSecretKey("badSecretKey");
        final ContractValidationException exception = assertThrows(ContractValidationException.class, input::validate);
        assertThat(exception.getCode()).isEqualTo(INVALID_SECRET_KEY.code);
        assertThat(exception.getMessage()).isEqualTo(INVALID_SECRET_KEY.message);
    }

    @Test
    public void validateWhenSecretKeySizeLessThan256BitsTest() {
        final VerifyTokenInput input = new VerifyTokenInput();
        input.setToken("token");
        input.setSecretKey(Base64.getEncoder().encodeToString("badSecretKey".getBytes(StandardCharsets.UTF_8)));
        final ContractValidationException exception = assertThrows(ContractValidationException.class, input::validate);
        assertThat(exception.getCode()).isEqualTo(INVALID_SECRET_KEY.code);
        assertThat(exception.getMessage()).isEqualTo(INVALID_SECRET_KEY.message);
    }

    @Test
    public void validateInput() throws ContractValidationException {
        final VerifyTokenInput input = new VerifyTokenInput();
        input.setToken("token");
        input.setSecretKey("tAC24j0OfEVFeXRH2ubfsrHNNzp/bxEmcaf5yWl108c=");

        input.validate();
        assertThat(input.toString()).isNotNull();
    }
}
