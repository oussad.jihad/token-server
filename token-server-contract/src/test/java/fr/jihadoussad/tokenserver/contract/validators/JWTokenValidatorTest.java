package fr.jihadoussad.tokenserver.contract.validators;

import fr.jihadoussad.tokenserver.test.TestHelper;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class JWTokenValidatorTest {

    @Test
    public void validateWhenInvalidTest() {
        assertThat(JWTokenValidator.validate("token"))
                .isFalse();
    }

    @Test
    public void validateWhenJWTPartNotEncodedTest() {
        assertThat(JWTokenValidator.validate("h.p.s"))
                .isFalse();
    }

    @Test
    public void validateJWTokenTest() {
        assertThat(JWTokenValidator.validate(TestHelper.TOKEN))
                .isTrue();
    }
}
