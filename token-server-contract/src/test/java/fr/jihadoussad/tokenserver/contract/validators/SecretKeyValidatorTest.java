package fr.jihadoussad.tokenserver.contract.validators;

import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

import static org.assertj.core.api.Assertions.assertThat;

public class SecretKeyValidatorTest {

    @Test
    public void validateWhenSecretKeyNotEncodedTest() {
        assertThat(SecretKeyValidator.validate("secretKey")).isFalse();
    }

    @Test
    public void validateWhenSecretKeyLessThan256BitsTest() {
        assertThat(SecretKeyValidator.validate(Base64.getEncoder().encodeToString("secretKey".getBytes(StandardCharsets.UTF_8)))).isFalse();
    }

    @Test
    public void validateSecretKeyTest() {
        assertThat(SecretKeyValidator.validate(Base64.getEncoder().encodeToString("tAC24j0OfEVFeXRH2ubfsrHNNzp/bxEmcaf5yWl108c=".getBytes(StandardCharsets.UTF_8)))).isTrue();
    }
}
