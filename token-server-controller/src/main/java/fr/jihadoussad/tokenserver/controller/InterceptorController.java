package fr.jihadoussad.tokenserver.controller;

import fr.jihadoussad.tokenserver.contract.exceptions.ContractValidationException;
import fr.jihadoussad.tools.api.response.ErrorOutput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static fr.jihadoussad.tools.api.response.ResponseCode.*;

@RestControllerAdvice
public class InterceptorController {

    private final Logger logger = LoggerFactory.getLogger(InterceptorController.class);

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorOutput> internalError(final Exception e) {
        logger.error("Technical error was occurred: ", e);
        final ErrorOutput output =  new ErrorOutput();
        output.setMessage(TECHNICAL_ERROR.message);
        output.setCode(TECHNICAL_ERROR.code);
        logger.info("END - Process rollback...");
        return new ResponseEntity<>(output, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ResponseEntity<Void> httpRequestError(final HttpRequestMethodNotSupportedException e) {
        logger.debug(e.getMessage());
        return ResponseEntity.notFound().build();
    }

    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    public ResponseEntity<String> httpMediaTypeError(final HttpMediaTypeNotSupportedException e) {
        logger.debug(e.getMessage());
        return ResponseEntity.notFound().build();
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<Void> httpMessageNotReadableError(final HttpMessageNotReadableException e) {
        logger.debug(e.getMessage());
        return ResponseEntity.badRequest().build();
    }

    @ExceptionHandler(ContractValidationException.class)
    public ResponseEntity<ErrorOutput> contractViolation(final ContractValidationException e) {
        logger.info(e.toString());
        // Compute httpStatus
        final HttpStatus httpStatus = buildHttpStatus(e);

        // Prepare output
        final ErrorOutput output =  new ErrorOutput();
        output.setMessage(e.getMessage());
        output.setCode(e.getCode());

        // End process - return
        logger.info("END - Process finish with status {}", httpStatus);
        return new ResponseEntity<>(output, httpStatus);
    }

    private HttpStatus buildHttpStatus(final ContractValidationException e) {
        if (EXPIRED_TOKEN.code.equals(e.getCode()) || INVALID_TOKEN.code.equals(e.getCode())) {
            return HttpStatus.FORBIDDEN;
        } else {
            return HttpStatus.BAD_REQUEST;
        }
    }
}
