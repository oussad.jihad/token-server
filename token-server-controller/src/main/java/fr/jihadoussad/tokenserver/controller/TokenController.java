package fr.jihadoussad.tokenserver.controller;

import fr.jihadoussad.tokenserver.contract.TokenService;
import fr.jihadoussad.tokenserver.contract.exceptions.ContractValidationException;
import fr.jihadoussad.tokenserver.contract.input.Token;
import fr.jihadoussad.tokenserver.contract.input.GenerateTokenInput;
import fr.jihadoussad.tokenserver.contract.input.VerifyTokenInput;
import io.jsonwebtoken.Claims;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TokenController {

    private final Logger logger = LoggerFactory.getLogger(TokenController.class);

    private final TokenService tokenService;

    public TokenController(final TokenService tokenService) {
        this.tokenService = tokenService;
    }

    @Operation(description = "Generate a json web token", responses = {
                    @ApiResponse(responseCode = "200", description = "Token successfully generated"),
                    @ApiResponse(responseCode = "400", description = "Contract input violation"),
                    @ApiResponse(responseCode = "500", description = "Technical error")
    })
    @PostMapping(value = "/generate", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> generateToken(@RequestBody  final GenerateTokenInput input) throws ContractValidationException {
        logger.info("BEGIN - Try to generate a token:");

        final String jwt = tokenService.generateToken(input);

        logger.info("END - Token successfully generated: {}", jwt);

        return ResponseEntity.ok(jwt);
    }

    @Operation(description = "Extract json web token payload", responses = {
            @ApiResponse(responseCode = "200", description = "Token payload successfully extracted"),
            @ApiResponse(responseCode = "400", description = "Contract input violation"),
            @ApiResponse(responseCode = "500", description = "Technical error")
    })
    @PostMapping(value = "/payload", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Claims> extractTokenPayload(@RequestBody  final Token jwt) throws ContractValidationException {
        logger.info("BEGIN - Try to extract payload token");

        final Claims payload = tokenService.extractPayload(jwt);

        logger.info("END - Token payload successfully extracted");

        return ResponseEntity.ok(payload);
    }

    @Operation(description = "Verify a json web token", responses = {
            @ApiResponse(responseCode = "200", description = "Token valid"),
            @ApiResponse(responseCode = "400", description = "Contract input violation"),
            @ApiResponse(responseCode = "403", description = "Token invalid or expired"),
            @ApiResponse(responseCode = "500", description = "Technical error")
    })
    @PostMapping(value = "/verify", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Claims> verifyToken(@RequestBody  final VerifyTokenInput input) throws ContractValidationException {
        logger.info("BEGIN - Try to verify token");

        final Claims payload = tokenService.verifyToken(input);

        logger.info("END - Token successfully verified");

        return ResponseEntity.ok(payload);
    }

    @Operation(description = "Invalidate a json web token", responses = {
            @ApiResponse(responseCode = "200", description = "Token valid"),
            @ApiResponse(responseCode = "400", description = "Contract input violation"),
            @ApiResponse(responseCode = "500", description = "Technical error")
    })
    @PostMapping(value = "/invalidate", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> invalidateToken(@RequestBody  final Token jwt) throws ContractValidationException {
        logger.info("BEGIN - Try to invalidate the following token: {}", jwt);

        tokenService.invalidateToken(jwt);

        logger.info("END - Token successfully invalidate.");

        return ResponseEntity.ok().build();
    }
}
