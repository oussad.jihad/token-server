package fr.jihadoussad.tokenserver.controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "fr.jihadoussad.tokenserver.*")
public class TokenServerApplicationTest {

    public static void main(final String[] args) {
        SpringApplication.run(TokenServerApplicationTest.class);
    }
}
