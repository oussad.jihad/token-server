package fr.jihadoussad.tokenserver.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.jihadoussad.tokenserver.contract.TokenService;
import fr.jihadoussad.tokenserver.contract.exceptions.ContractValidationException;
import fr.jihadoussad.tokenserver.contract.input.Token;
import fr.jihadoussad.tokenserver.contract.input.GenerateTokenInput;
import fr.jihadoussad.tokenserver.contract.input.VerifyTokenInput;
import fr.jihadoussad.tools.api.response.ErrorOutput;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.impl.DefaultClaims;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;

import static fr.jihadoussad.tools.api.response.ResponseCode.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(TokenController.class)
public class TokenServerTest {

    private static final String GENERATE_URL = "/generate";
    private static final String PAYLOAD_URL = "/payload";
    private static final String VERIFY_URL = "/verify";
    private static final String INVALIDATE_URL = "/invalidate";

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private TokenService tokenService;

    @Test
    public void generateTokenWhenInternalErrorTest() throws Exception {
        when(tokenService.generateToken(any())).thenThrow(new RuntimeException());

        jsonResponse(GENERATE_URL, new GenerateTokenInput(), status().isInternalServerError());
    }

    @Test
    public void generateTokenWhenMissingMandatoryFieldTest() throws Exception {
        when(tokenService.generateToken(any())).thenThrow(new ContractValidationException(MISSING_MANDATORY_FIELD.message, MISSING_MANDATORY_FIELD.code));

        final String jsonResponse = jsonResponse(GENERATE_URL, new GenerateTokenInput(), status().isBadRequest());
        final ErrorOutput response = objectMapper.readValue(jsonResponse, ErrorOutput.class);
        assertThat(response.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
        assertThat(response.getMessage()).isEqualTo(MISSING_MANDATORY_FIELD.message);
    }

    @Test
    public void generateTokenTest() throws Exception {
        when(tokenService.generateToken(any())).thenReturn("token");

        final String jsonResponse = jsonResponse(GENERATE_URL, new GenerateTokenInput(), status().isOk());
        assertThat(jsonResponse).isEqualTo("token");
    }

    @Test
    public void extractPayloadWhenInternalErrorTest() throws Exception {
        when(tokenService.extractPayload(any())).thenThrow(new RuntimeException());

        final String jsonResponse = jsonResponse(PAYLOAD_URL, new Token(), status().isInternalServerError());
        final ErrorOutput response = objectMapper.readValue(jsonResponse, ErrorOutput.class);
        assertThat(response.getCode()).isEqualTo(TECHNICAL_ERROR.code);
        assertThat(response.getMessage()).isEqualTo(TECHNICAL_ERROR.message);
    }

    @Test
    public void extractPayloadWhenMissingMandatoryFieldTest() throws Exception {
        when(tokenService.extractPayload(any())).thenThrow(new ContractValidationException(MISSING_MANDATORY_FIELD.message, MISSING_MANDATORY_FIELD.code));

        final String jsonResponse = jsonResponse(PAYLOAD_URL, new Token(), status().isBadRequest());
        final ErrorOutput response = objectMapper.readValue(jsonResponse, ErrorOutput.class);
        assertThat(response.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
        assertThat(response.getMessage()).isEqualTo(MISSING_MANDATORY_FIELD.message);
    }

    @Test
    public void extractPayloadTest() throws Exception {
        final Claims payload = new DefaultClaims();
        when(tokenService.extractPayload(any())).thenReturn(payload);

        final String jsonResponse = jsonResponse(PAYLOAD_URL, new Token(), status().isOk());
        final Claims response = objectMapper.readValue(jsonResponse, DefaultClaims.class);
        assertThat(response).isEqualTo(payload);
    }

    @Test
    public void verifyTokenWhenInternalErrorTest() throws Exception {
        when(tokenService.verifyToken(any())).thenThrow(new RuntimeException());

        final String jsonResponse = jsonResponse(VERIFY_URL, new VerifyTokenInput(), status().isInternalServerError());
        final ErrorOutput response = objectMapper.readValue(jsonResponse, ErrorOutput.class);
        assertThat(response.getCode()).isEqualTo(TECHNICAL_ERROR.code);
        assertThat(response.getMessage()).isEqualTo(TECHNICAL_ERROR.message);
    }

    @Test
    public void verifyTokenWhenMissingMandatoryFieldTest() throws Exception {
        when(tokenService.verifyToken(any())).thenThrow(new ContractValidationException(MISSING_MANDATORY_FIELD.message, MISSING_MANDATORY_FIELD.code));

        final String jsonResponse = jsonResponse(VERIFY_URL, new VerifyTokenInput(), status().isBadRequest());
        final ErrorOutput response = objectMapper.readValue(jsonResponse, ErrorOutput.class);
        assertThat(response.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
        assertThat(response.getMessage()).isEqualTo(MISSING_MANDATORY_FIELD.message);
    }

    @Test
    public void verifyTokenWhenInvalidTest() throws Exception {
        when(tokenService.verifyToken(any())).thenThrow(new ContractValidationException(INVALID_TOKEN.message, INVALID_TOKEN.code));

        final String jsonResponse = jsonResponse(VERIFY_URL, new VerifyTokenInput(), status().isForbidden());
        final ErrorOutput response = objectMapper.readValue(jsonResponse, ErrorOutput.class);
        assertThat(response.getCode()).isEqualTo(INVALID_TOKEN.code);
        assertThat(response.getMessage()).isEqualTo(INVALID_TOKEN.message);
    }

    @Test
    public void verifyTokenWhenExpiredTest() throws Exception {
        when(tokenService.verifyToken(any())).thenThrow(new ContractValidationException(EXPIRED_TOKEN.message, EXPIRED_TOKEN.code));

        final String jsonResponse = jsonResponse(VERIFY_URL, new VerifyTokenInput(), status().isForbidden());
        final ErrorOutput response = objectMapper.readValue(jsonResponse, ErrorOutput.class);
        assertThat(response.getCode()).isEqualTo(EXPIRED_TOKEN.code);
        assertThat(response.getMessage()).isEqualTo(EXPIRED_TOKEN.message);
    }

    @Test
    public void verifyTokenTest() throws Exception {
        final Claims payload = new DefaultClaims();
        when(tokenService.verifyToken(any())).thenReturn(payload);

        final String jsonResponse = jsonResponse(VERIFY_URL, new VerifyTokenInput(), status().isOk());
        final Claims response = objectMapper.readValue(jsonResponse, DefaultClaims.class);
        assertThat(response).isEqualTo(payload);
    }

    @Test
    public void invalidateTokenWhenInternalErrorTest() throws Exception {
        doThrow(new RuntimeException()).when(tokenService).invalidateToken(any());

        final String jsonResponse = jsonResponse(INVALIDATE_URL, new Token(), status().isInternalServerError());
        final ErrorOutput response = objectMapper.readValue(jsonResponse, ErrorOutput.class);
        assertThat(response.getCode()).isEqualTo(TECHNICAL_ERROR.code);
        assertThat(response.getMessage()).isEqualTo(TECHNICAL_ERROR.message);
    }

    @Test
    public void invalidateTokenWhenMissingMandatoryFieldTest() throws Exception {
        doThrow(new ContractValidationException(MISSING_MANDATORY_FIELD.message, MISSING_MANDATORY_FIELD.code)).when(tokenService).invalidateToken(any());

        final String jsonResponse = jsonResponse(INVALIDATE_URL, new Token(), status().isBadRequest());
        final ErrorOutput response = objectMapper.readValue(jsonResponse, ErrorOutput.class);
        assertThat(response.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
        assertThat(response.getMessage()).isEqualTo(MISSING_MANDATORY_FIELD.message);
    }

    @Test
    public void invalidateTokenTest() throws Exception {
        jsonResponse(INVALIDATE_URL, new Token(), status().isOk());
    }

    private String jsonResponse(final String urlTemplate, final Object input, final ResultMatcher resultMatcher) throws Exception {
        return mvc.perform(post(urlTemplate)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(input)))
                .andExpect(resultMatcher)
                .andReturn()
                .getResponse()
                .getContentAsString();
    }
}
