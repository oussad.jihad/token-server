package fr.jihadoussad.tokenserver.core.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * The token entity purged
 */
@Entity
public class InvalidToken {

    @Id
    @GeneratedValue
    private Long id;

    @Column(length = 512)
    private String token;

    public Long getId() {
        return id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
