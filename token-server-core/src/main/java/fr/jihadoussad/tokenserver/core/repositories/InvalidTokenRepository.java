package fr.jihadoussad.tokenserver.core.repositories;

import fr.jihadoussad.tokenserver.core.entities.InvalidToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InvalidTokenRepository extends JpaRepository<InvalidToken, Long> {

    InvalidToken findInvalidTokenByToken(final String token);

    void deleteInvalidTokenByToken(final String token);
}
