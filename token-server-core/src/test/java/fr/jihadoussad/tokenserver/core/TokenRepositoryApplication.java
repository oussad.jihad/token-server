package fr.jihadoussad.tokenserver.core;

import fr.jihadoussad.tokenserver.core.entities.InvalidToken;
import fr.jihadoussad.tokenserver.core.repositories.InvalidTokenRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan(basePackages = "fr.jihadoussad.tokenserver.core.*")
@EnableJpaRepositories
public class TokenRepositoryApplication {

    public static final String TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";

    public static void main(String[] args) {
        SpringApplication.run(TokenRepositoryApplication.class);
    }

    @Bean
    public CommandLineRunner loadData(final InvalidTokenRepository invalidTokenRepository) {
        return (args) -> {
            // create new application
            final InvalidToken invalidToken = new InvalidToken();
            invalidToken.setToken(TOKEN);
            invalidTokenRepository.save(invalidToken);

            invalidTokenRepository.flush();
        };
    }

}
