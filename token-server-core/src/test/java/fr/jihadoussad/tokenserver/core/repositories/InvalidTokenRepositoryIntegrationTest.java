package fr.jihadoussad.tokenserver.core.repositories;

import fr.jihadoussad.tokenserver.core.entities.InvalidToken;
import fr.jihadoussad.tokenserver.core.TokenRepositoryApplication;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class InvalidTokenRepositoryIntegrationTest {

    @Resource
    private InvalidTokenRepository invalidTokenRepository;

    @Test
    public void checkSetUpTest() {
        assertThat(invalidTokenRepository.findAll())
                .isNotEmpty()
                .hasSize(1);
    }

    @Test
    public void invalidateTokenTest() {
        final InvalidToken invalidToken = invalidTokenRepository.findInvalidTokenByToken(TokenRepositoryApplication.TOKEN);

        assertThat(invalidToken.getToken())
                .isEqualTo(TokenRepositoryApplication.TOKEN);
    }

    @Test
    public void purgeTokenExpiredTest() {
        invalidTokenRepository.deleteInvalidTokenByToken(TokenRepositoryApplication.TOKEN);

        assertThat(invalidTokenRepository.findInvalidTokenByToken(TokenRepositoryApplication.TOKEN))
                .isNull();
    }
}
