package fr.jihadoussad.tokenserver.jackson;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.impl.DefaultClaims;

import java.io.IOException;

public class ClaimsDeserializer extends JsonDeserializer<Claims> {

    @Override
    public Claims deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        return jsonParser.readValueAs(DefaultClaims.class);
    }
}
