package fr.jihadoussad.tokenserver.jackson;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.impl.DefaultClaims;
import org.junit.jupiter.api.Test;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

public class ClaimsDeserializerTest {

    @Test
    public void deserializeTest() throws JsonProcessingException {
        final Claims claims = new DefaultClaims();
        claims.setExpiration(new Date());
        claims.setSubject("toto");
        claims.setId("123456");

        final SimpleModule module = new SimpleModule();
        module.addDeserializer(Claims.class, new ClaimsDeserializer());

        final ObjectMapper objectMapper = Jackson2ObjectMapperBuilder
                .json()
                .serializationInclusion(JsonInclude.Include.NON_NULL)
                .modules(module)
                .build();

        final String jsonResult = objectMapper.writeValueAsString(claims);
        final Claims objectResult = objectMapper.readValue(jsonResult, Claims.class);

        assertThat(objectResult).isNotNull();
        assertThat(objectResult.getId()).isEqualTo(claims.getId());
        assertThat(objectResult.getSubject()).isEqualTo(claims.getSubject());
        assertThat(objectResult.getExpiration()).isEqualTo(claims.getExpiration());
    }
}
