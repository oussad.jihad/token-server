package fr.jihadoussad.tokenserver.provider;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.jihadoussad.tokenserver.contract.TokenService;
import fr.jihadoussad.tokenserver.contract.exceptions.ContractValidationException;
import fr.jihadoussad.tokenserver.contract.input.Token;
import fr.jihadoussad.tokenserver.contract.input.GenerateTokenInput;
import fr.jihadoussad.tokenserver.contract.input.VerifyTokenInput;
import fr.jihadoussad.tokenserver.core.entities.InvalidToken;
import fr.jihadoussad.tokenserver.core.repositories.InvalidTokenRepository;
import fr.jihadoussad.tools.security.KeyUtils;
import io.jsonwebtoken.*;
import io.jsonwebtoken.impl.DefaultClaims;
import io.jsonwebtoken.security.SignatureException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Base64;
import java.util.Date;
import java.util.UUID;

import static fr.jihadoussad.tools.api.response.ResponseCode.*;

@Service
public class JWTokenService implements TokenService {

    private final Logger logger = LoggerFactory.getLogger(JWTokenService.class);

    private final InvalidTokenRepository invalidTokenRepository;

    private final ObjectMapper objectMapper;

    public JWTokenService(final InvalidTokenRepository invalidTokenRepository, final ObjectMapper objectMapper) {
        this.invalidTokenRepository = invalidTokenRepository;
        this.objectMapper = objectMapper;
    }

    @Override
    public String generateToken(final GenerateTokenInput input) throws ContractValidationException {
        // Validate input
        logger.debug("Validate input to generate token: {}", input.toString());
        input.validate();

        // Process and return
        final Date now = new Date();
        final Date exp = new Date(System.currentTimeMillis() + input.getExpiration());
        final SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
        logger.debug("Generate jwt with builder...");
        final String jwt =  Jwts.builder()
            .setId(UUID.randomUUID().toString().replace("-", ""))
            .setIssuer(input.getIssuer())
            .setIssuedAt(now)
            .setNotBefore(now)
            .setExpiration(exp)
            .setSubject(input.getSubject())
            .addClaims(input.getClaims())
            .signWith(KeyUtils.stringToKey(input.getSecretKey()), signatureAlgorithm)
            .compact();
        logger.debug("Jwt generated: {}", jwt);
        return jwt;
    }

    @Override
    public Claims extractPayload(final Token jwt) throws ContractValidationException {
        Claims claims = null;
        try {
            // Validate input
            logger.debug("Validate token input to extract: {}", jwt.toString());
            jwt.validate();

            //Process
            final String[] jwtArray = jwt.getToken().split("\\.");
            final String payload = jwtArray[1];
            logger.debug("extracting payload with following json: {}", payload);
            claims = objectMapper.readValue(Base64.getDecoder().decode(payload), DefaultClaims.class);
        } catch (final IOException e) {
            logger.warn("The token cannot be read as json web token... Returning null payload");
        } catch (final ContractValidationException e) {
            if (INVALID_TOKEN.code.equals(e.getCode())) {
                logger.info("The token cannot be read as json web token... Returning null payload");
            } else {
                logger.debug(e.toString());
                throw e;
            }
        }

        // Return
        logger.debug("Following claims returned: {}", claims);
        return claims;
    }

    @Override
    public Claims verifyToken(final VerifyTokenInput input) throws ContractValidationException {
        // Validate
        logger.debug("Validate token input to verify: {}", input.toString());
        input.validate();

        try {
            // Check if the token has been invalidate
            if (invalidTokenRepository.findInvalidTokenByToken(input.getToken()) != null) {
                logger.info("Following token found in the invalid token repository: {}", input.getToken());
                throw new ContractValidationException(INVALID_TOKEN.message, INVALID_TOKEN.code);
            }

            // Process
            logger.debug("Check token in progress...");
            final Claims claims = Jwts.parser()
                .setSigningKey(input.getSecretKey())
                .parseClaimsJws(input.getToken())
                .getBody();
            logger.debug("Token ok ! Claims extracted: {}", claims);

            // Verify mandatory fields
            if (claims.getSubject() == null || claims.getIssuer() == null) {
                logger.warn("[HACKED] The issuer or subject token extracted is null");
                throw new ContractValidationException(INVALID_TOKEN.message, INVALID_TOKEN.code);
            }

            // Return
            return claims;
        } catch (final UnsupportedJwtException | MalformedJwtException | SignatureException e) {
            throw new ContractValidationException(INVALID_TOKEN.message, INVALID_TOKEN.code);
        } catch (final ExpiredJwtException e) {
            throw new ContractValidationException(EXPIRED_TOKEN.message, EXPIRED_TOKEN.code);
        }
    }

    @Override
    public void invalidateToken(final Token jwt) throws ContractValidationException {
        try {
            // Validate input
            logger.debug("Validate token input to invalidate: {}", jwt.toString());
            jwt.validate();

            // Check
            if (invalidTokenRepository.findInvalidTokenByToken(jwt.getToken()) != null) {
                logger.info("Following token already invalid: {}", jwt.getToken());
                return;
            }

            // Persist
            final InvalidToken invalidToken = new InvalidToken();
            invalidToken.setToken(jwt.getToken());
            logger.debug("Process following invalid persistence token: {}", invalidToken);
            invalidTokenRepository.save(invalidToken);
            invalidTokenRepository.flush();
        } catch (final ContractValidationException exception) {
            if (INVALID_TOKEN.code.equals(exception.getCode())) {
                logger.info(TOKEN_ALREADY_INVALID.message);
            } else {
                throw exception;
            }
        }
    }
}
