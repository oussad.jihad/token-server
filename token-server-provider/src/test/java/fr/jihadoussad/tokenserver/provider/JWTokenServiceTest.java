package fr.jihadoussad.tokenserver.provider;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.jihadoussad.tokenserver.contract.TokenService;
import fr.jihadoussad.tokenserver.contract.exceptions.ContractValidationException;
import fr.jihadoussad.tokenserver.contract.input.Token;
import fr.jihadoussad.tokenserver.contract.input.GenerateTokenInput;
import fr.jihadoussad.tokenserver.contract.input.VerifyTokenInput;
import fr.jihadoussad.tokenserver.core.entities.InvalidToken;
import fr.jihadoussad.tokenserver.core.repositories.InvalidTokenRepository;
import fr.jihadoussad.tokenserver.test.TestHelper;
import fr.jihadoussad.tools.security.KeyUtils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.io.Encoders;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import javax.crypto.Mac;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import static fr.jihadoussad.tools.api.response.ResponseCode.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class JWTokenServiceTest {

    @Autowired
    private TokenService tokenService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private InvalidTokenRepository tokenRepository;

    private Token jwt;

    /*
        On configure ici un jeton jwt généré par l'AuthorizationService à 1 seconde d'expiration,
        un id généré aléatoirement, et une clé secrète Hmac_SHA256 généré par notre SecretKeyGenerator.
    */

    @BeforeEach
    public void setUp() throws ContractValidationException {
        final GenerateTokenInput input = new GenerateTokenInput();
        input.setSubject(TestHelper.SUBJECT);
        input.setIssuer(TestHelper.ISSUER);
        input.setSecretKey(TestHelper.SECRET_KEY);
        input.setExpiration(10000L); // 10 secondes
        input.setClaims(Map.of(TestHelper.CLAIM_KEY, TestHelper.CLAIM_VALUE));

        jwt = new Token();
        jwt.setToken(tokenService.generateToken(input));
    }

    @AfterEach
    public void clear() {
        tokenRepository.deleteAll();
        tokenRepository.flush();
    }

    @Test
    public void generateTokenWhenSecretKeyInvalidTest() {
        final GenerateTokenInput input = new GenerateTokenInput();
        input.setSecretKey(Base64.getEncoder().encodeToString("secretKey".getBytes(StandardCharsets.UTF_8)));

        final ContractValidationException exception = assertThrows(ContractValidationException.class, () -> tokenService.generateToken(input));
        assertThat(exception.getCode()).isEqualTo(INVALID_SECRET_KEY.code);
    }

    /*
        Nous vérifions dans ce test si nous arrivons à générer correctement un jeton et
        qu'à partir de la clé secrète, nous récupérons bien nos informations dans le payload.
    */

    @Test
    public void generateTokenTest() throws NoSuchAlgorithmException, InvalidKeyException {
        final String[] jwtArray = jwt.getToken().split("\\.");
        final String header = jwtArray[0];
        final String payload = jwtArray[1];
        final String feeder = header+"."+payload;

        final Mac sha256HMAC = Mac.getInstance("HmacSHA256");
        sha256HMAC.init(KeyUtils.stringToKey(TestHelper.SECRET_KEY));
        final String signatureExpected = Encoders.BASE64URL.encode(sha256HMAC.doFinal(feeder.getBytes()));

        assertThat(signatureExpected).isEqualTo(jwtArray[2]);
    }

    @Test
    public void extractPayloadWhenTokenNullTest() {
        final Token input = new Token();

        final ContractValidationException exception = assertThrows(ContractValidationException.class, () -> tokenService.extractPayload(input));
        assertThat(exception.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
    }

    @Test
    public void extractPayloadWhenTokenInvalidTest() throws ContractValidationException {
        final Token input = new Token();
        input.setToken("token");

        final Claims payload = tokenService.extractPayload(input);
        assertThat(payload).isNull();
    }

    @Test
    public void extractPayloadWhenPayloadInvalidTest() throws ContractValidationException {
        final Token input = new Token();
        input.setToken("header.fakePayload.signature");

        final Claims payload = tokenService.extractPayload(input);
        assertThat(payload).isNull();
    }

    @Test
    public void extractPayloadTest() throws ContractValidationException {
        final Claims payload = tokenService.extractPayload(jwt);
        assertThat(payload).isNotNull();
        assertThat(payload.getSubject()).isEqualTo(TestHelper.SUBJECT);
        assertThat(payload.getIssuer()).isEqualTo(TestHelper.ISSUER);
        assertThat(payload.get(TestHelper.CLAIM_KEY)).isEqualTo(TestHelper.CLAIM_VALUE);
    }

    @Test
    public void verifyTokenWhenSecretKeyInvalidTest() {
        final VerifyTokenInput input = new VerifyTokenInput();
        input.setToken(jwt.getToken());
        input.setSecretKey(Base64.getEncoder().encodeToString("secretKey".getBytes(StandardCharsets.UTF_8)));

        final ContractValidationException exception = assertThrows(ContractValidationException.class, () -> tokenService.verifyToken(input));
        assertThat(exception.getCode()).isEqualTo(INVALID_SECRET_KEY.code);
    }

    @Test
    public void verifyTokenWhenNoIssuerExtractedTest() throws NoSuchAlgorithmException, InvalidKeyException {
        final VerifyTokenInput input = new VerifyTokenInput();
        // Split the token to extract header
        final String header = jwt.getToken().split("\\.")[0];
        final String payload = Base64.getEncoder().encodeToString("{\"iss\":\"devilIssuer\"}".getBytes(StandardCharsets.UTF_8));
        final String feeder = header + "." + payload;

        final Mac sha256HMAC = Mac.getInstance("HmacSHA256");
        sha256HMAC.init(KeyUtils.stringToKey(TestHelper.SECRET_KEY));
        final String signature = Encoders.BASE64URL.encode(sha256HMAC.doFinal(feeder.getBytes()));

        input.setToken(header + "." + payload + "." + signature);
        input.setSecretKey(TestHelper.SECRET_KEY);

        final ContractValidationException exception = assertThrows(ContractValidationException.class, () -> tokenService.verifyToken(input));
        assertThat(exception.getCode()).isEqualTo(INVALID_TOKEN.code);
    }

    @Test
    public void verifyTokenWhenNoSubjectExtractedTest() throws NoSuchAlgorithmException, InvalidKeyException {
        final VerifyTokenInput input = new VerifyTokenInput();
        // Split the token to extract header
        final String header = jwt.getToken().split("\\.")[0];
        final String payload = Base64.getEncoder().encodeToString("{\"sub\":\"devilSubject\"}".getBytes(StandardCharsets.UTF_8));
        final String feeder = header + "." + payload;

        final Mac sha256HMAC = Mac.getInstance("HmacSHA256");
        sha256HMAC.init(KeyUtils.stringToKey(TestHelper.SECRET_KEY));
        final String signature = Encoders.BASE64URL.encode(sha256HMAC.doFinal(feeder.getBytes()));

        input.setToken(header + "." + payload + "." + signature);
        input.setSecretKey(TestHelper.SECRET_KEY);

        final ContractValidationException exception = assertThrows(ContractValidationException.class, () -> tokenService.verifyToken(input));
        assertThat(exception.getCode()).isEqualTo(INVALID_TOKEN.code);
    }

    @Test
    public void verifyTokenTest() throws ContractValidationException {
        final VerifyTokenInput input = new VerifyTokenInput();
        input.setToken(jwt.getToken());
        input.setSecretKey(TestHelper.SECRET_KEY);

        final Claims payload = tokenService.verifyToken(input);

        assertThat(payload.getSubject()).isEqualTo(TestHelper.SUBJECT);
        assertThat(payload.getIssuer()).isEqualTo(TestHelper.ISSUER);
        assertThat(payload.get(TestHelper.CLAIM_KEY)).isEqualTo(TestHelper.CLAIM_VALUE);
    }

    /*
        Ce test nous permet de vérifier si l'on peut casser le jeton via la faille "Signature Stripping".
        Celle-ci consiste à decoder le header, de positionner l'algo en non-signé, et de renvoyer le jeton sans la signature
        tout en altérant le payload si on le souhaite.
    */

    @Test
    public void signatureStripingTest() throws ContractValidationException, JsonProcessingException {
        final Map<String, String> header = new HashMap<>();
        header.put("alg", "none");

        final VerifyTokenInput input = new VerifyTokenInput();
        input.setToken(jwt.getToken());
        input.setSecretKey(TestHelper.SECRET_KEY);
        final Claims payload = tokenService.verifyToken(input);
        payload.put("privileges", "adminTest");

        final String alterJwt = Base64.getEncoder().encodeToString(objectMapper.writeValueAsBytes(header))+ "."
                + Base64.getEncoder().encodeToString(objectMapper.writeValueAsBytes(payload)) + ".";

        final VerifyTokenInput verifyTokenInput = new VerifyTokenInput();
        verifyTokenInput.setToken(alterJwt);
        verifyTokenInput.setSecretKey(TestHelper.SECRET_KEY);

        final ContractValidationException exception = assertThrows(ContractValidationException.class, () ->  tokenService.verifyToken(verifyTokenInput));

        assertThat(exception.getCode()).isEqualTo(INVALID_TOKEN.code);
        assertThat(exception.getMessage()).isEqualTo(INVALID_TOKEN.message);
    }

    @Test
    public void alterTokenTest() throws JsonProcessingException, ContractValidationException {
        final String[] arrayJwt = jwt.getToken().split("\\.");

        final VerifyTokenInput input = new VerifyTokenInput();
        input.setToken(jwt.getToken());
        input.setSecretKey(TestHelper.SECRET_KEY);
        final Claims payload = tokenService.verifyToken(input);
        payload.put("privileges", "adminTest");

        final String alterJwt = arrayJwt[0] + "." + Base64.getEncoder().encodeToString(objectMapper.writeValueAsBytes(payload)) + "."
                + arrayJwt[2];

        final VerifyTokenInput verifyTokenInput = new VerifyTokenInput();
        verifyTokenInput.setToken(alterJwt);
        verifyTokenInput.setSecretKey(TestHelper.SECRET_KEY);

        final ContractValidationException exception = assertThrows(ContractValidationException.class, () ->  tokenService.verifyToken(verifyTokenInput));

        assertThat(exception.getCode()).isEqualTo(INVALID_TOKEN.code);
        assertThat(exception.getMessage()).isEqualTo(INVALID_TOKEN.message);
    }

    @Test
    public void tokenExpireTest() {
        final VerifyTokenInput verifyTokenInput = new VerifyTokenInput();
        verifyTokenInput.setToken(TestHelper.TOKEN_EXPIRED);
        verifyTokenInput.setSecretKey(TestHelper.SECRET_KEY);

        final ContractValidationException exception = assertThrows(ContractValidationException.class, () ->  tokenService.verifyToken(verifyTokenInput));

        assertThat(exception.getCode()).isEqualTo(EXPIRED_TOKEN.code);
        assertThat(exception.getMessage()).isEqualTo(EXPIRED_TOKEN.message);
    }

    @Test
    public void invalidateTokenWhenNullTest() {
        final ContractValidationException exception = assertThrows(ContractValidationException.class, () -> tokenService.invalidateToken(new Token()));

        assertThat(exception.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
        assertThat(exception.getMessage()).isEqualTo(MISSING_MANDATORY_FIELD.message + "token null");
    }

    @Test
    public void invalidateTokenTest() throws ContractValidationException {
        tokenService.invalidateToken(jwt);

        final VerifyTokenInput verifyTokenInput = new VerifyTokenInput();
        verifyTokenInput.setToken(jwt.getToken());
        verifyTokenInput.setSecretKey(TestHelper.SECRET_KEY);

        final ContractValidationException exception = assertThrows(ContractValidationException.class, () -> tokenService.verifyToken(verifyTokenInput));

        assertThat(exception.getCode()).isEqualTo(INVALID_TOKEN.code);
        assertThat(exception.getMessage()).isEqualTo(INVALID_TOKEN.message);
    }

    @Test
    public void invalidateTokenWhenInvalidTest() throws ContractValidationException {
        final Token fakeJwt = new Token();
        fakeJwt.setToken("token");
        tokenService.invalidateToken(fakeJwt);
    }

    @Test
    public void invalidateTokenWhenAlreadyInvalidTest() throws ContractValidationException {
        final InvalidToken invalidToken = new InvalidToken();
        invalidToken.setToken(jwt.getToken());
        tokenRepository.save(invalidToken);
        tokenRepository.flush();

        tokenService.invalidateToken(jwt);
    }
}
