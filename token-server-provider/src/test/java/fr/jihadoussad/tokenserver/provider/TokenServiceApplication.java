package fr.jihadoussad.tokenserver.provider;

import fr.jihadoussad.tokenserver.core.entities.InvalidToken;
import fr.jihadoussad.tokenserver.core.repositories.InvalidTokenRepository;
import fr.jihadoussad.tokenserver.test.TestHelper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = "fr.jihadoussad.tokenserver.*")
@EntityScan(basePackages = "fr.jihadoussad.tokenserver.core.*")
@EnableJpaRepositories(basePackages = "fr.jihadoussad.tokenserver.core.*")
public class TokenServiceApplication {

    public static void main(final String[] args) {
        SpringApplication.run(TokenServiceApplication.class);
    }

    @Bean
    public CommandLineRunner loadData(final InvalidTokenRepository invalidTokenRepository) {
        return (args) -> {
            // create new application
            final InvalidToken invalidToken = new InvalidToken();
            invalidToken.setToken(TestHelper.TOKEN);
            invalidTokenRepository.save(invalidToken);

            invalidTokenRepository.flush();
        };
    }
}
