package fr.jihadoussad.tokenserver.test;

public final class TestHelper {

    public static final String CLAIM_KEY = "claim";
    public static final String CLAIM_VALUE = "claim";
    public static final String ISSUER = "issuer";
    public static final String SUBJECT = "subject";
    public static final String SECRET_KEY = "xzW80+/XTAVcbo/Ba8wSma/onAoOvWin8DLCzzwJeD4=";
    public static final String TOKEN = "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJiZDI0MTE0NDFhZDA0YjU4OGJiZjIwOGRkNTliMzg0NSIsImlzcyI6Imlzc3VlciIsImlhdCI6MTYxMzUwMDQxNSwibmJmIjoxNjEzNTAwNDE1LCJleHAiOjMzMTcwNDI2NDE1LCJzdWIiOiJzdWJqZWN0IiwiY2xhaW0iOiJjbGFpbSJ9.5OWa4Emd_SVfqYbUc7jYvOB5ttH4uxEiWP1KoVIKjzk";
    public static final String TOKEN_CLAIMS_INVALID = "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJiZDI0MTE0NDFhZDB0YjU4OGLiZjIwOGRkNTliMzg0NSIsImlzcyI6Imlzc3VlciIsImlhdCI6MTYxMzUwMDQxNSwibmJmIjoxNjEzNTAwNDE1LCJleHAiOjMzMTcwNDI2NDE1LCJzdWIiOiJzdWJqZWN0IiwiY2xhaW0iOiJjbGFpbSJ9.5OWa4Emd_SVfqYbUc7jYvOB5ttH4uxEiWP1KoVIKj5A";
    public static final String TOKEN_EXPIRED = "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI4ZjdiZTI2MDk3NWM0NzE2ODBiNjBiZDIwYTg3ZTYyYyIsImlzcyI6Imlzc3VlciIsImlhdCI6MTYxMzUwMDE5NCwibmJmIjoxNjEzNTAwMTk0LCJleHAiOjE2MTM1MDAxOTUsInN1YiI6InN1YmplY3QiLCJjbGFpbSI6ImNsYWltIn0.AVCYbQLcpIShu8gWjf0m_2fd9XWLxVdzAcgZqKaDp5I";
}
